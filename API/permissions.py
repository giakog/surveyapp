from rest_framework import permissions
from django.core.exceptions import PermissionDenied
from polls.models import *


class IsSameUserOrReadOnly(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return obj.user.id == request.user.id


class IsUserLogged(permissions.BasePermission):
    def has_permission(self, request, view):
        return bool(request.user and request.user.is_authenticated)


class IsSurveyPossessorOrReadOnly(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return obj.creator_id == request.user.id


class IsSurveyPossessor(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.user.is_authenticated and obj.creator_id == request.user.id:
            return True
        return False


class IsSubmissionPossessor(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return obj.user_id == request.user.id


class IsSurveyComponentPossessor(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return obj.survey.creator_id == request.user.id

