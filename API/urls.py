from django.urls import path
from . import views

app_name = 'API'

urlpatterns = [
    path('category/list', views.CategoryList.as_view(), name='API-category-list'),
    path('tag/list', views.TagList.as_view(), name='APi-tags-list'),
    path('survey/list/<int:c_id>', views.SurveyList.as_view(), name='API-survey-list'),
    path('survey/list/<int:c_id>/filtered-by/<int:tag>', views.TagFilteredSurveyList.as_view(), name='API-survey-list'),
    path('survey/list/<int:c_id>/sorted-by/<str:key>/order/<int:order>', views.FieldSortedSurveyList.as_view(), name='API-survey-list'),
    path('survey/list/<int:c_id>/search/', views.InputFilteredSurveyList.as_view(), name='API-survey-list'),
    path('survey/create', views.CreateSurvey.as_view(), name='API-survey-create'),
    path('survey/<int:pk>/stats/<str:q>/<str:c>/<str:cs>/<str:ed>/<str:ws>/<str:gen>/<str:country>/<str:city>',
         views.SurveyStats.as_view(), name='API-survey-stats'),
    path('profile/search/<str:username>', views.ProfileGet.as_view(), name='API-profile-create'),
    path('profile/create', views.ProfileCreate.as_view(), name='API-profile-create'),
    path('profile/my', views.MyProfile.as_view(), name='API-profile'),
    path('survey/list/my', views.MySurveyList.as_view(), name='API-survey-my'),
    path('survey/list/my/filtered-by/<int:tag>', views.MyTagFilteredSurveyList.as_view(), name='API-survey-list'),
    path('survey/list/my/sorted-by/<str:key>/order/<int:order>', views.MyFieldSortedSurveyList.as_view(),
         name='API-survey-list'),
    path('survey/list/my/search/', views.MyInputFilteredSurveyList.as_view(), name='API-survey-list'),
    path('submission/list/my', views.MySubmissionList.as_view(), name='API-submission-my'),
    path('survey/<int:pk>/my', views.MySubmissionDetail.as_view(), name='API-submission-my'),
    path('survey/detail/<int:pk>/creator', views.DetailedSurveyCreator.as_view(), name='API-survey-creator-detail'),
    path('survey/detail/<int:pk>/submitter', views.DetailedSurveySubmitter.as_view(), name='API-survey-submitter-detail'),
    path('survey/<int:pk>/edit', views.EditSurvey.as_view(), name='API-survey-edit'),
    path('survey/<int:pk>/question/create', views.CreateQuestion.as_view(), name='API-question-create'),
    path('survey/<int:pk>/question/<int:id>/edit', views.EditQuestion.as_view(), name='API-question-edit'),
    path('survey/<int:pk>/question/<int:id>/choice/create', views.CreateChoice.as_view(), name='API-choice-create'),
    path('survey/<int:pk>/question/<int:id>/choice/<int:c_id>/edit', views.EditChoice.as_view(), name='API-choice-edit'),
    path('survey/<int:pk>/question/<int:id>/choice-list', views.QuestionChoiceList.as_view(), name='API-question-choice-list'),
    path('survey/<int:pk>/submission-start', views.SubmissionCreate.as_view(), name='API-submission-new'),
    path('submission/<int:pk>/answers-create', views.AnswersCreate.as_view(), name='API-submission-answers-new'),
]
