from rest_framework.response import Response
from rest_framework import generics
from .permissions import *
from .serializers import *
# Create your views here.
from django.db.models import Count, Q
from django.core.exceptions import PermissionDenied
from rest_framework.views import APIView


class CategoryList(generics.ListAPIView):
    """
    Questa view restituisce la lista completa degli utenti registrati
    """
    serializer_class = CategorySerializer

    def get_queryset(self):
        return Category.objects.all()


class TagList(generics.ListAPIView):
    serializer_class = TagSerializer

    def get_queryset(self):
        return Tag.objects.all()


class ProfileCreate(generics.CreateAPIView):
    permission_classes = [IsUserLogged]
    serializer_class = CurrentProfile


class ProfileGet(generics.RetrieveAPIView):

    serializer_class = CurrentProfile

    def get_object(self):
        name = self.kwargs['username']
        profile = Profile.objects.get(user__username=name)
        profile.user.password = ""
        print(profile)
        return profile


class MyProfile(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsSameUserOrReadOnly, IsUserLogged]
    serializer_class = CurrentProfile

    def get_object(self):
        profile = Profile.objects.get(user_id=self.request.user)
        profile.user.password = ""
        print(profile)
        return profile


class MySurveyList(generics.ListAPIView):
    permission_classes = [IsUserLogged]
    serializer_class = SurveyListSerializer

    def get_queryset(self):
        return Survey.objects.filter(creator=self.request.user)


class MyTagFilteredSurveyList(generics.ListAPIView):
    """
    Questa view restituisce la lista completa degli utenti registrati
    """
    permission_classes = [IsUserLogged]
    serializer_class = SurveyListSerializer

    def get_queryset(self):
        """
        Modifico il query set in modo da ottenere le query attive
        """
        t_id = self.kwargs['tag']
        return Survey.objects.filter(tags__id__icontains=t_id, creator=self.request.user)


class MyInputFilteredSurveyList(generics.ListAPIView):
    """
    Questa view restituisce la lista completa degli utenti registrati
    """
    permission_classes = [IsUserLogged]
    serializer_class = SurveyListSerializer

    def get_queryset(self):
        """
        Modifico il query set in modo da ottenere le query attive
        """
        txt = self.request.GET['q']
        return Survey.objects.filter(Q(creator=self.request.user) &
                                     Q(status__exact=1) & (Q(title__contains=txt) | Q(description__contains=txt)))


class MyFieldSortedSurveyList(generics.ListAPIView):
    """
    Questa view restituisce la lista completa degli utenti registrati
    """
    permission_classes = [IsUserLogged]
    serializer_class = SurveyListSerializer

    def get_queryset(self):
        """
        Modifico il query set in modo da ottenere le query attive
        """
        fil = self.kwargs['key']
        ord = self.kwargs['order']
        if fil == "submissions":
            if ord == 0:
                return Survey.objects.filter(creator=self.request.user).order_by('-subs')
            else:
                return Survey.objects.filter(creator=self.request.user).order_by('subs')

        if fil == "date":
            if ord == 0:
                return Survey.objects.filter(creator=self.request.user).order_by('-date')
            else:
                return Survey.objects.filter(creator=self.request.user).order_by('date')


class MySubmissionList(generics.ListAPIView):
    permission_classes = [IsUserLogged]
    serializer_class = DetailedSurveySubmitterSerializer

    def get_queryset(self):
        return Survey.objects.filter(submissions__user=self.request.user, submissions__is_complete=True)


class MySubmissionDetail(generics.RetrieveAPIView):
    serializer_class = SubmissionSerializer
    permission_classes = [IsSurveyComponentPossessor, IsUserLogged]

    def get_object(self):
        print(self.request.user)
        s_id = self.kwargs['pk']
        return Submission.objects.get(survey=s_id, user=self.request.user, is_complete=True)


class SurveyList(generics.ListAPIView):
    """
    Questa view restituisce la lista completa degli utenti registrati
    """
    serializer_class = SurveyListSerializer

    def get_queryset(self):
        """
        Modifico il query set in modo da ottenere le query attive
        """
        c_id = self.kwargs['c_id']
        if c_id == 8:
            return Survey.objects.filter(status=1)
        else:
            return Survey.objects.filter(category_id=c_id, status=1)


class TagFilteredSurveyList(generics.ListAPIView):
    """
    Questa view restituisce la lista completa degli utenti registrati
    """
    serializer_class = SurveyListSerializer

    def get_queryset(self):
        """
        Modifico il query set in modo da ottenere le query attive
        """
        c_id = self.kwargs['c_id']
        t_id = self.kwargs['tag']
        if c_id == 8:
            return Survey.objects.filter(tags__id__icontains=t_id, status=1)
        else:
            return Survey.objects.filter(category_id=c_id, tags__id__icontains=t_id, status=1)


class InputFilteredSurveyList(generics.ListAPIView):
    """
    Questa view restituisce la lista completa degli utenti registrati
    """
    serializer_class = SurveyListSerializer

    def get_queryset(self):
        """
        Modifico il query set in modo da ottenere le query attive
        """
        c_id = self.kwargs['c_id']
        txt = self.request.GET['q']
        print(txt)
        if c_id == 8:
            return Survey.objects.filter(Q(status__exact=1) & (Q(title__contains=txt) | Q(description__contains=txt)))
        else:
            return Survey.objects.filter(Q(category__exact=c_id) &
                                         Q(status__exact=1) & (Q(title__contains=txt) | Q(description__contains=txt)))


class FieldSortedSurveyList(generics.ListAPIView):
    """
    Questa view restituisce la lista completa degli utenti registrati
    """
    serializer_class = SurveyListSerializer

    def get_queryset(self):
        """
        Modifico il query set in modo da ottenere le query attive
        """
        c_id = self.kwargs['c_id']
        fil = self.kwargs['key']
        ord = self.kwargs['order']
        if fil == "submissions":
            if c_id == 8:
                if ord == 0:
                    return Survey.objects.filter(status=1).order_by('-subs')
                else:
                    return Survey.objects.filter(status=1).order_by('subs')
            else:
                if ord == 0:
                    return Survey.objects.filter(status=1).order_by('-subs')
                else:
                    return Survey.objects.filter(status=1).order_by('subs')
        if fil == "date":
            if c_id == 8:
                if ord == 0:
                    return Survey.objects.filter(status=1).order_by('-date')
                else:
                    return Survey.objects.filter(status=1).order_by('date')
            else:
                if ord == 0:
                    return Survey.objects.filter(category_id=c_id, status=1).order_by('-date')
                else:
                    return Survey.objects.filter(category_id=c_id, status=1).order_by('date')


class DetailedSurveyCreator(generics.RetrieveDestroyAPIView):
    permission_classes = [IsSurveyPossessor, IsUserLogged]
    serializer_class = DetailedSurveyCreatorSerializer

    def get_object(self):
        s_id = self.kwargs['pk']
        survey = Survey.objects.get(id=s_id, creator=self.request.user)
        user_n = User.objects.get(id=survey.creator.id)
        survey.creator_user = user_n
        return survey

    def perform_destroy(self, instance):
        print(instance)
        survey_d = Survey.objects.get(id=instance.id)
        if survey_d.creator == self.request.user:
            survey_d.delete()
        else:
            raise PermissionDenied()


class DetailedSurveySubmitter(generics.RetrieveAPIView):
    serializer_class = DetailedSurveySubmitterSerializer
    permission_classes = [IsSurveyPossessorOrReadOnly]

    def get_object(self):
        s_id = self.kwargs['pk']
        survey = Survey.objects.get(id=s_id)
        user_n = User.objects.get(id=survey.creator.id)
        survey.creator_user = user_n
        return survey


class CreateSurvey(generics.CreateAPIView):
    permission_classes = [IsUserLogged]
    serializer_class = SingleSurveyEditSerializer


class EditSurvey(generics.RetrieveUpdateAPIView):
    permission_classes = [IsSurveyPossessorOrReadOnly, IsUserLogged]
    serializer_class = SingleSurveyEditSerializer

    def get_object(self):
        s_id = self.kwargs['pk']
        survey = Survey.objects.get(id=s_id, creator=self.request.user)
        return survey


class CreateQuestion(generics.CreateAPIView):
    permission_classes = [IsUserLogged]
    serializer_class = QuestionEditSerializer


class EditQuestion(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsSurveyComponentPossessor]
    serializer_class = QuestionEditSerializer

    def get_object(self):
        print(self.request.user)
        s_id = self.kwargs['pk']
        q_id = self.kwargs['id']
        question = Question.objects.get(id=q_id, survey=s_id)
        return question

    def perform_destroy(self, instance):
        question_d = Question.objects.get(id=instance.id, survey_id=instance.survey_id)
        if question_d.survey.creator == self.request.user:
            question_d.delete()
        else:
            raise PermissionDenied()


class CreateChoice(generics.CreateAPIView):
    serializer_class = ChoiceEditSerializer
    permission_classes = [IsUserLogged]


class EditChoice(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ChoiceEditSerializer
    permission_classes = [IsSurveyComponentPossessor, IsUserLogged]

    def get_object(self):
        s_id = self.kwargs['pk']
        q_id = self.kwargs['id']
        c_id = self.kwargs['c_id']
        choice = Choice.objects.get(id=c_id, survey=s_id, question=q_id)
        print(choice)
        return choice

    def perform_destroy(self, instance):
        choice_d = Choice.objects.get(id=instance.id)
        if choice_d.survey.creator == self.request.user:
            choice_d.delete()
        else:
            raise PermissionDenied()


class QuestionChoiceList(generics.ListAPIView):
    serializer_class = ChoicesSerializer

    def get_queryset(self):
        s_id = self.kwargs['pk']
        q_id = self.kwargs['id']
        return Choice.objects.filter(survey_id=s_id, question_id=q_id)


class SubmissionCreate(generics.CreateAPIView):
    permission_classes = [IsUserLogged]
    serializer_class = SingleSubmissionSerializer


class AnswersCreate(generics.CreateAPIView):
    permission_classes = [IsUserLogged]
    serializer_class = AnswerSerializer


def get_filtered_profiles(prof_1, f, i, c_id):
    if i == 2:
        prof_1 = prof_1.prefetch_related('user__submissions__answers') \
            .filter(civil_state=f[i],
                    user__submission__answers__in=Answer.objects.filter(choice_id=c_id))
    if i == 3:
        prof_1 = prof_1.prefetch_related('user__submissions__answers') \
            .filter(education=f[i],
                    user__submission__answers__in=Answer.objects.filter(choice_id=c_id))
    if i == 4:
        prof_1 = prof_1.prefetch_related('user__submissions__answers') \
            .filter(working_state=f[i],
                    user__submission__answers__in=Answer.objects.filter(choice_id=c_id))
    if i == 5:
        prof_1 = prof_1.prefetch_related('user__submissions_answers') \
            .filter(sex=f[i],
                    user__submission__answers__in=Answer.objects.filter(choice_id=c_id))
    if i == 6:
        print(f[i])
        if f[i] != "ZZ":
            prof_1 = prof_1.prefetch_related('user__submissions__answers') \
                .filter(country=f[i],
                        user__submission__answers__in=Answer.objects.filter(choice_id=c_id))
    if i == 7:
        print(f[i])
        if f[i] != "all" and f[i] != "All" and f[i] != "ALL":
            prof_1 = prof_1.prefetch_related('user__submissions_answers') \
                .filter(city=f[i],
                        user__submission__answers__in=Answer.objects.filter(choice_id=c_id))
    return prof_1


def group_profiles(prof, indx, grp, f, c_id):
    prof_1 = prof
    tot = 0
    txt = []
    for i in indx:
        if i != 0 and i != 1 and f[i] != "X":
            prof_1 = get_filtered_profiles(prof_1, f, i, c_id)

    if len(grp) == 1:
        tot = prof_1.values(grp[0]).annotate(totale=Count(grp[0])).order_by(grp[0])
    elif len(grp) == 2:
        tot = prof_1.values(grp[0], grp[1]).annotate(totale=Count(grp[0])).order_by(grp[0],grp[1])
    elif len(grp) == 3:
        tot = prof_1.values(grp[0], grp[1], grp[2]).annotate(totale=Count(grp[0])).order_by(grp[0], grp[1], grp[2])
    elif len(grp) == 4:
        tot = prof_1.values(grp[0], grp[1], grp[2], grp[3]).\
            annotate(totale=Count(grp[0])).order_by(grp[0], grp[1], grp[2], grp[3])
    elif len(grp) == 5:
        tot = prof_1.values(grp[0], grp[1], grp[2], grp[3], grp[4]). \
            annotate(totale=Count(grp[0])).order_by(grp[0], grp[1], grp[2], grp[3], grp[4])
    elif len(grp) == 6:
        tot = prof_1.values(grp[0], grp[1], grp[2], grp[3], grp[4], grp[5]). \
            annotate(totale=Count(grp[0])).order_by(grp[0], grp[1], grp[2], grp[3], grp[4], grp[5])
    elif len(grp) == 7:
        tot = prof_1.values(grp[0], grp[1], grp[2], grp[3], grp[4], grp[5], grp[6]). \
            annotate(totale=Count(grp[0])).order_by(grp[0], grp[1], grp[2], grp[3], grp[4], grp[5], grp[6])
    elif len(grp) == 8:
        tot = prof_1.values(grp[0], grp[1], grp[2], grp[3], grp[4], grp[5], grp[6], grp[7]). \
            annotate(totale=Count(grp[0])).order_by(grp[0], grp[1], grp[2], grp[3], grp[4], grp[5], grp[6], grp[7])
    return tot


def main_survey_loop(indx, opt, questions, grp, f):
    tot_ret = []
    if opt == 2:
        for question in questions:
            if f[1] != "0":
                choice = Choice.objects.get(question_id=question.id, id=int(f[1]))
                prof = Profile.objects.prefetch_related('user__submissions__answers') \
                    .filter(user__submission__answers__in=Answer.objects.filter(question_id=question.id, choice_id=int(f[1])))
                tot = group_profiles(prof, indx, grp, f, int(f[1]))
                for t in tot:
                    t['Question'] = question.text
                    t['Choice'] = choice.text
                    t['id'] = choice.id
                    tot_ret.append(t)
            else:
                for choice in question.choices.all():
                    prof = Profile.objects.prefetch_related('user__submissions__answers') \
                        .filter(user__submission__answers__in=Answer.objects.filter(question_id=question.id,
                                                                                     choice_id=choice.id))
                    tot = group_profiles(prof, indx, grp, f, choice.id)
                    for t in tot:
                        t['Question'] = question.text
                        t['Choice'] = choice.text
                        t['id'] = choice.id
                        tot_ret.append(t)
    else:
        if f[1] != "0":
            choice = Choice.objects.get(question_id=questions.id, id=int(f[1]))
            prof = Profile.objects.prefetch_related('user__submission_set__answer_set') \
                .filter(
                user__submission__answers__in=Answer.objects.filter(question_id=questions.id, choice_id=int(f[1])))
            tot = group_profiles(prof, indx, grp, f, int(f[1]))
            print("choice:", choice.text, tot)
            if tot != 0:
                for t in tot:
                    t['Question'] = questions.text
                    t['Choice'] = choice.text
                    t['id'] = choice.id
                    tot_ret.append(t)

        else:
            for choice in questions.choices.all():
                prof = Profile.objects.prefetch_related('user__submissions__answers') \
                    .filter(user__submission__answers__in=Answer.objects.filter(question_id=questions.id,
                                                                               choice_id=choice.id))
                tot = group_profiles(prof, indx, grp, f, choice.id)
                for t in tot:
                    t['Question'] = questions.text
                    t['Choice'] = choice.text
                    t['id'] = choice.id
                    tot_ret.append(t)
    print(tot_ret)
    return tot_ret


class SurveyStats(APIView):

    def get(self, request, *args, **kwargs):
        try:
            survey = Survey.objects.prefetch_related("questions__choices").get(
                Q(id__exact=kwargs.get('pk')) & (Q(status__exact=1) | Q(status__exact=2))
                & Q(creator__exact=self.request.user))
        except Survey.DoesNotExist:
            return Response({"Error": "Survey not found"})

        f_l = ["question", "choice", "civil_state", "education", "working_state", "sex", "country", "city"]
        f_l_1 = ["Domanda", "Opzione", "Stato Civile", "Educazione", "Stato lavorativo", "Genere", "Paese", "Città"]
        f = [kwargs.get('q'), kwargs.get('c'), kwargs.get('cs'), kwargs.get('ed'), kwargs.get('ws'),
             kwargs.get('gen'), kwargs.get('country'), kwargs.get('city')]
        c = 0
        indx = []
        for i in f:
            if i != "0" and i != "null":
                indx.append(c)
            c += 1
        print(indx, c)
        '''Check if there is a filter on the question'''
        if f[0] != '0':
            q = int(f[0])
        else:
            q = 0
        if f[1] != '0':
            c = int(f[1])
        else:
            c = 0
        if len(indx) == 0:
            return Response(
                {"Error": "Nessuna filtro specificato oppure campo opzione specificato senza campo domanda"})

        elif len(indx) == 1:
            if f[0] != "0" or f[1] != "0":
                return Response(
                    {"Error": "Nessuna filtro specificato oppure campo opzione specificato senza campo domanda"})
            else:
                grp = [f_l[indx[0]]]
                questions = survey.questions.all()
                tot_ret = main_survey_loop(indx, 2, questions, grp, f)
        elif len(indx) >= 2:
            grp = []
            for x in indx:
                if x != 0 and x != 1:
                    grp.append(f_l[x])
            if f[0] == "0" and f[1] != "0":
                return Response(
                    {"Error": "Nessuna filtro specificato oppure campo opzione specificato senza campo domanda"})
            if f[0] != "0" and f[1] != "0":
                questions = survey.questions.get(id=int(f[0]))
                tot_ret = main_survey_loop(indx, 1, questions, grp, f)
            elif f[0] != "0" and f[1] == "0":
                questions = survey.questions.get(id=int(f[0]))
                tot_ret = main_survey_loop(indx, 1, questions, grp, f)
            elif f[0] == "0" and f[1] == "0":
                questions = survey.questions.all()
                tot_ret = main_survey_loop(indx, 2, questions, grp, f)
        fields = []
        for i in indx:
            if i != 0 and i != 1:
                fields.append(f_l_1[i])
            fields.append('Totale')
        for t in tot_ret:
            if "Question" in t:
                t["domanda"] = t.pop("Question")
            if "Choice" in t:
                t["opzione"] = t.pop("Choice")
            if "sex" in t:
                t["genere"] = t.pop("sex")
            if "civil_state" in t:
                t["stato civile"] = t.pop("civil_state")
            if "education" in t:
                t["educazione"] = t.pop("education")
            if "working_state" in t:
                t["stato lavorativo"] = t.pop("working_state")
            if "country" in t:
                t["paese"] = t.pop("country")
            if "city" in t:
                t["citta"] = t.pop("city")

        return Response({"fields": fields, "stats": tot_ret} )




