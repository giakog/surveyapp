from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from django.contrib.auth.models import User
from polls.models import *
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient


# Create your tests here.
class listAPIViewTestCase(APITestCase):
    def setUp(self):
        self.client = APIClient()

        self.user = User.objects.create_user(username='user', email='user@mail.com', password='12345')
        self.token_user = Token.objects.create(user=self.user)
        self.profilo = Profile.objects.create(user_id=self.user.id, sex='maschio', civil_state='single',
                                              working_state='studente', education='elementare', country='IT',
                                              city='Modena')
        self.profilo.save()

        self.survey = Survey.objects.create(creator=self.user.id, title='Titolo', description='Descrizione',
                                            thanks_message='Grazie', category=1, tags=[1, 2], slug='Titolo', status=0)
        self.question = Question.objects.create(survey_id=self.survey.id, text='testo', type=0, other_field=False)
        self.choice = Choice.objects.create(question_id=self.question.id, text='Testo',)

        self.submission = Submission.objects.create(user_id=self.user.id, survey_id=self.survey.id, is_complete=False)
        self.answer = Answer.objects.create(submission_id=self.submission.id, question_id=self.question.id,
                                            choice_id=self.choice.id)

    def testAccountCreate(self):
        url = "http://192.168.1.34:8000/api/rest-auth/registration/"
        data = {
            "username": "usr1",
            "email": "usr1@mail.com",
            "password1": "12345",
            "password2": "12345",
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def testAccountLogin(self):
        url = "http:/192.168.1.34:8000/api/rest-auth/login/"
        data = {
            "username": "user",
            "password": "12345"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def testAccountProfileCreate(self):
        url = reverse("API:API-profile-create")
        data = {
            "sex": "maschio",
            "civil_state": "single",
            "working_state": "studente",
            "education": "elementare",
            "country": "IT",
            "city": "Modena"
        }
        self.client.credentials(HTTP_AUTHORIZATION='Token' + self.token_user.key)
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def testSurveyCreate(self):
        url = reverse("API:API-survey-create")
        data = {
            "creator": self.user.id,
            "title": "Titolo",
            "description": "Descrizione",
            "thanks_message": "Grazie",
            "category": 1,
            "tags": [1, 2],
            "status": 0,
        }
        self.client.credentials(HTTP_AUTHORIZATION='Token' + self.token_user.key)
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def testQuestionCreate(self):
        url = reverse("API:API-question-create")
        data = {
            "survey": self.survey.id,
            "text": "Testo",
            "type": 0,
            "other_field": False
        }
        self.client.credentials(HTTP_AUTHORIZATION='Token' + self.token_user.key)
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def testChoiceCreate(self):
        url = reverse("API:API-choice-create")
        data = {
            "survey": self.survey.id,
            "question": self.question.id,
            "text": "Testo"
        }
        self.client.credentials(HTTP_AUTHORIZATION='Token' + self.token_user.key)
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def testSubmissionCreate(self):
        url = reverse("API:API-submission-new")
        data = {
            "survey": self.survey.id,
            "is_complete": 1
        }
        self.client.credentials(HTTP_AUTHORIZATION='Token' + self.token_user.key)
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def testAnswerCreate(self):
        url = reverse("API:API-submission-answers-new")
        data = {
            "submission": self.submission.id,
            "choice": self.choice.id,
            "question": self.question.id,
            "extra_text": "##"
        }
        self.client.credentials(HTTP_AUTHORIZATION='Token' + self.token_user.key)
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def testSurveyList(self):
        url = reverse("API:API-survey-list", kwargs={"c_id": 1})
        response = self.client.post(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def testSurveyListMy(self):
        url = reverse("API:API-survey-my")
        self.client.credentials(HTTP_AUTHORIZATION='Token' + self.token_user.key)
        response = self.client.post(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def testSubmissionListMy(self):
        url = reverse("API:API-submission-my")
        self.client.credentials(HTTP_AUTHORIZATION='Token' + self.token_user.key)
        response = self.client.post(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def testSurveyStats(self):
        url = reverse("API:API-survey-stats", kwargs={
            "pk": self.survey.id,
            "q": 0,
            "c": 0,
            "cs": 0,
            "ws": 0,
            "ed": "X",
            "gen": "X",
            "country": "null",
            "city": "null"
        })
        self.client.credentials(HTTP_AUTHORIZATION='Token' + self.token_user.key)
        response = self.client.post(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
