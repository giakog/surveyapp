from rest_framework import serializers
from rest_framework.response import Response
from django.contrib.auth.models import User
from django.contrib.auth.validators import UnicodeUsernameValidator
from polls.models import *
from django.db.models import Count
from django.utils import timezone


def answer_count(answer):
    subs = Answer.objects.prefetch_related("choice__answer_set").filter(choice_id=answer, submission__is_complete=True)\
        .values('choice_id').annotate(count=Count('choice_id'))
    return subs


def submission_count(survey):
    sub = Submission.objects.filter(survey=survey.id, is_complete=True).values('id').annotate(count=Count('id'))
    print(sub)
    return sub


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = "__all__"


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = "__all__"


class CreatorSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username']


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        exclude = [
                "last_login",
                "is_superuser",
                "is_staff",
                "is_active",
                "date_joined",
                "groups",
                "user_permissions",
                "password",
                "email",
                "first_name",
                "last_name"
        ]
        read_only_fields = ["id", "username"]


class CurrentProfile(serializers.ModelSerializer):
    user = UserSerializer(many=False, read_only=True)

    class Meta:
        model = Profile
        fields = ["user", "sex", "civil_state", "education", "working_state", "country", "city"]
        read_only_fields = ("id", "user")

    def update(self, instance, validated_data):
        instance.sex = validated_data['sex']
        instance.working_state = validated_data['working_state']
        instance.education = validated_data['education']
        instance.civil_state = validated_data['civil_state']
        instance.country = validated_data['country']
        instance.city = validated_data['city']
        instance.save()
        return instance

    def create(self, validated_data):
        profile_new = Profile.objects.create(
            user=self.context['request'].user,
            sex=validated_data['sex'],
            working_state=validated_data['working_state'],
            education=validated_data['education'],
            civil_state=validated_data['civil_state'],
            country=validated_data['country'],
            city=validated_data['city']
        )
        profile_new.save()
        return profile_new


class SurveyListSerializer(serializers.ModelSerializer):
    tags = TagSerializer(read_only=True, many=True)
    sub_completed = serializers.SerializerMethodField('get_count')
    category = CategorySerializer(read_only=True)

    def get_count(self, survey):
        return submission_count(survey)

    class Meta:
        model = Survey
        fields = ['id', 'creator', 'title', 'category', 'description', 'tags', 'date', 'status', 'sub_completed', 'subs']
        read_only_fields = ["id", "creator"]


class AnswerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Answer
        fields = ['id', 'submission', 'choice', 'question', 'extra_text']
        read_only_fields = ("id",)

    def create(self, validated_data):
        print(validated_data)
        new_answer = Answer.objects.create(
            submission=validated_data['submission'],
            choice=validated_data['choice'],
            question=validated_data['question'],
        )
        if validated_data['extra_text'] == "##":
            new_answer.extra_text = ""
        else:
            new_answer.extra_text = validated_data['extra_text']
        new_answer.save()
        return new_answer


class SingleSubmissionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Submission
        read_only = ["id"]
        fields = ['id', 'survey', 'is_complete']

    def create(self, validated_data):
        print(validated_data)
        submission_new = Submission.objects.create(
            user=self.context['request'].user,
            survey=validated_data['survey'],
            is_complete=validated_data['is_complete']
        )
        srv = Survey.objects.get(id=validated_data['survey'].id)
        srv.subs = Submission.objects.filter(survey=validated_data['survey'].id, is_complete=True).count() + 1
        srv.save()
        submission_new.save()
        return submission_new


class SubmissionSerializer(serializers.ModelSerializer):
    answers = AnswerSerializer(many=True)

    class Meta:
        model = Submission
        read_only_fields = ['id']
        fields = "__all__"

    def create(self, validated_data):
        print(validated_data)
        submission_data = validated_data.pop('submission')
        submission_new = Submission.objects.create(
            user=self.context['request'].user,
            survey=submission_data['survey'],
            is_complete=submission_data['is_complete']
        )
        submission_new.save()
        for i in range(len(validated_data['answers'])):
            if validated_data['extra_text'] != "":
                answer_new = Answer.objects.create(
                    submission_id=submission_new.pk,
                    quesiton_id=validated_data['questions'][i],
                    choice_id=validated_data['choice'][i],
                    extra_text=validated_data['extra_text'],
                )
            else:
                answer_new = Answer.objects.create(
                    submission_id=submission_new.pk,
                    quesiton_id=validated_data['questions'][i],
                    choice_id=validated_data['choice'][i],
                    extra_text="",
                )
            answer_new.save()
        return submission_new


class ChoicesSerializer(serializers.ModelSerializer):
    count = serializers.SerializerMethodField('get_count')

    class Meta:
        model = Choice
        fields = ['id', 'text', 'count']

    def get_count(self, choice):
        return answer_count(choice)


class QuestionSerializer(serializers.ModelSerializer):
    choices = ChoicesSerializer(many=True, read_only=True)

    class Meta:
        model = Question
        fields = ['id', 'text', 'type', 'other_field', 'choices']


class DetailedSurveyCreatorSerializer(serializers.ModelSerializer):
    questions = QuestionSerializer(many=True, read_only=False)
    creator_user = CreatorSerializer(many=False, read_only=False)
    submissions = SubmissionSerializer(many=True, read_only=False)
    sub_completed = serializers.SerializerMethodField('get_count')
    category = CategorySerializer(read_only=True)
    tags = TagSerializer(read_only=True, many=True)

    def get_count(self, survey):
        return submission_count(survey)

    class Meta:
        model = Survey
        fields = ['id', 'title', 'creator', 'creator_user', 'description', 'thanks_message', 'category', 'tags', 'date',
                  'status', 'questions', 'submissions', 'sub_completed']


class DetailedSurveySubmitterSerializer(serializers.ModelSerializer):
    questions = QuestionSerializer(many=True, read_only=True)
    creator_user = CreatorSerializer(many=False, read_only=True)
    category = CategorySerializer(read_only=True)
    tags = TagSerializer(many=True, read_only=True)

    class Meta:
        model = Survey
        fields = ['id', 'title', 'creator', 'creator_user', 'description', 'category', 'tags', 'date', 'status',
                  'questions', 'thanks_message']


class SingleSurveyEditSerializer(serializers.ModelSerializer):

    class Meta:
        model = Survey
        read_only_fields = ['id', 'creator']
        fields = ['id', 'creator', 'title', 'description', 'thanks_message', 'category', 'tags', 'status']

    def update(self, instance, validated_data):
        instance.title = validated_data['title']
        instance.description = validated_data['description']
        instance.category = validated_data['category']
        new_tags = validated_data['tags']
        instance.status = validated_data['status']
        instance.date = timezone.now()
        if new_tags:
            instance.tags.clear()
            for tag in new_tags:
                instance.tags.add(tag)
        instance.save()
        if instance.status == "1":
            questions = Question.objects.filter(survey_id=instance.pk)
            for q in questions:
                if q.type == "0" and q.other_field:
                    new_choice = Choice.objects.create(
                        survey_id=instance.pk,
                        question_id=q.id,
                        text="Altro")
                    new_choice.save()
        return instance

    def create(self, validated_data):
        n_s = Survey.objects.all().count()
        print(validated_data['category'])
        tags = validated_data['tags']
        survey_new = Survey.objects.create(
            creator=self.context['request'].user,
            title=validated_data['title'],
            thanks_message=validated_data['thanks_message'],
            description=validated_data['description'],
            category=validated_data['category'],
            status=validated_data['status'],
            date=timezone.now(),
            slug=validated_data['title'].strip() + str(n_s)
        )
        if tags:
            for tag in tags:
                survey_new.tags.add(tag)
        survey_new.save()
        return survey_new


class QuestionEditSerializer(serializers.ModelSerializer):

    class Meta:
        model = Question
        read_only_fields = ['id']
        fields = ['id', 'text', 'type', 'other_field', 'survey']

    def update(self, instance, validated_data):
        if instance.type != validated_data['type']:
            for i in Choice.objects.filter(survey=validated_data['survey'], question=instance.id):
                i.delete()
            if validated_data['type'] == "1":
                for i in range(5):
                    choice = Choice.objects.create(survey=validated_data['survey'],
                                                   question_id=instance.id, text=str(i + 1))
                    choice.save()

        if instance.other_field == 1 and validated_data['other_field'] == 0:
            c = Choice.objects.get(survey=validated_data['survey'], question=instance.id, text="Altro")
            c.delete()
        instance.survey = validated_data['survey']
        instance.text = validated_data['text']
        instance.type = validated_data['type']
        instance.other_field = validated_data['other_field']
        instance.save()
        return instance

    def create(self, validated_data):
        print(validated_data)
        if Survey.objects.get(id=validated_data['survey'].id, creator=self.context['request'].user):
            question_new = Question.objects.create(
                survey=validated_data['survey'],
                text=validated_data['text'],
                type=validated_data['type'],
                other_field=validated_data['other_field'],
            )
            question_new.save()
            if question_new.type == '1':
                for i in range(5):
                    choice = Choice.objects.create(survey_id=question_new.survey_id, question_id=question_new.pk)
                    choice.text = str(i + 1)
                    choice.save()
            return question_new
        else:
            return Response({"Errore": "Operazione non consentita."})


class ChoiceEditSerializer(serializers.ModelSerializer):

    class Meta:
        model = Choice
        read_only_fields = ['id']
        fields = ['id', 'survey', 'question', 'text']

    def update(self, instance, validated_data):
        instance.survey = validated_data['survey']
        instance.question = validated_data['question']
        instance.text = validated_data['text']
        instance.save()
        print(instance)
        return instance

    def create(self, validated_data):
        if Survey.objects.get(id=validated_data['survey'].id, creator=self.context['request'].user):
            choice_new = Choice.objects.create(
                survey=validated_data['survey'],
                question=validated_data['question'],
                text=validated_data['text'],
            )
            choice_new.save()
            return choice_new
        else:
            return Response({"Errore": "Operazione non consentita"})
