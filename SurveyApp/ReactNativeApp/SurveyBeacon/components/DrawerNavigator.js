import React, { Component } from 'react';
import {Text, View, SafeAreaView, ScrollView, Image, Button, TouchableOpacity} from 'react-native';
import { createDrawerNavigator, DrawerItems } from 'react-navigation-drawer';
import { Icon } from 'native-base';
import { Dimensions } from 'react-native';
import CategoryList from '../pages/CategoryList'
import Login from '../pages/Login.js'
import Logout from '../pages/Logout';
import logo from '../assets/favicon.png';
import SurveyCreate from "../pages/SurveyCreate";
import SurveyStackNavigator from './SurveyStackNavigator';
import ProfileStackNavigator from './ProfileStackNavigator';
import Registration from "../pages/Registration";
import MySubmissionListSN from "./MySubmissionListStackNavigator";
const {width, height} = Dimensions.get('window');

const hiddenDrawerItems = ['NestedDrawerNavigator'];

const CustomDrawerNavigation = (props) => {

    let label_user = "";
    if (!global.logged_in) {
      label_user = "Utente anonimo";
    } else {
      label_user = global.username;
    }
    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={{ height: 250, backgroundColor: '#cfe3f3', opacity: 0.9 }}>
                <View style={{ backgroundColor: 'Green', alignItems: 'center', justifyContent: 'center' }}>
                    <Text>{label_user}</Text>
                     { global.logged_in == true ? (
                         <View style={{marginTop: 10}}>
                             <Text style={{fontWeight: 'bold'}}>Dati profilo</Text>
                             <Text>Genere: {global.gender}</Text>
                             <Text>Stato civile: {global.cs}</Text>
                             <Text>Educazione: {global.ed}</Text>
                             <Text>Stato lavorativo: {global.ws}</Text>
                             <Text>Stato: {global.country}</Text>
                             <Text>Città: {global.city}</Text>
                         </View>
                     ): null }
                </View>
          </View>
          <ScrollView>
              <DrawerItems {...props}/>
          </ScrollView>
            <View style={{ alignItems: "center", backgroundColor: '#e7e7e7' }}>
                <View style={{ flexDirection: 'row' }}>
                    <Image source={logo} style={{ width: 25, height: 25 }}  />
                    <Text style={{paddingTop: 2, fontFamily: 'typold-medium', color: '#7e7777'}}> GILIOLI</Text>
                    <Text style={{paddingTop: 3, color: '#7e7777'}}> - 2021</Text>
                </View>
            </View>
        </SafeAreaView>
    );
};

const Drawer = createDrawerNavigator({
     CategoryList: {
        screen: CategoryList,
        navigationOptions: {
        title: 'Categorie',
        drawerIcon: ({ tintColor }) => (
            <Icon name = "md-clipboard" />
          )
        }

    },
    SurveyCreate: {
         screen: SurveyCreate,
         navigationOptions: ({navigation}) => {
             if(!global.logged_in) {
                 return {
                     drawerLabel: () => null
                 }
             } else {
                 return {
                     title: 'Crea sondaggio',
                     drawerIcon: ({ tintColor }) => (
                         <Icon name = "md-add" />
                         )
                 }
             }
         }
    },
    SurveyStackNavigator: {
         screen: SurveyStackNavigator,
        navigationOptions: ({navigation}) => {
             if(!global.logged_in) {
                 return {
                     drawerLabel: () => null
                 }
             } else {
                 return {
                     title: 'Miei Sondaggi',
                     drawerIcon: ({ tintColor }) => (
                         <Icon name = "md-person" />
                         )
                 }
             }
         }
    },
    MySubmissionListSN: {
         screen: MySubmissionListSN,
                navigationOptions: ({navigation}) => {
             if(!global.logged_in) {
                 return {
                     drawerLabel: () => null
                 }
             } else {
                 return {
                     title: 'Mie Sottomissioni',
                     drawerIcon: ({ tintColor }) => (
                         <Icon name = "md-person" />
                         )
                 }
             }
         }

    },
     Registration: {
        screen: Registration,
        navigationOptions: ({navigation}) => {
          if(global.logged_in) {
            return {
              drawerLabel: () => null
            }
          } else {
            return {
              title: 'Registrazione',
              drawerIcon: ({ tintColor }) => (
                  <Icon name = "md-pencil" />
              )
            }
          }
        }
    },

    ProfileStackNavigator: {
         screen: ProfileStackNavigator,
        navigationOptions: ({navigation}) => {
             if(!global.logged_in) {
            return {
                drawerLabel: () => null
            }
            } else {
                return {
                    title: 'Modifica profilo',
                    drawerIcon: ({ tintColor }) => (
                        <Icon name = "md-person" />
                        )
                }
            }
         }
     },

    Login: {
        screen: Login,
        navigationOptions: ({navigation}) => {
          if(global.logged_in) {
            return {
              drawerLabel: () => null
            }
          } else {
            return {
              title: 'Login',
              drawerIcon: ({ tintColor }) => (
                <Icon name = "md-power" />
              )
            }
          }
        }
    },
    Logout: {
        screen: Logout,
        navigationOptions: ({navigation}) => {
            if (!global.logged_in) {
                return {
                    drawerLabel: () => null
                }
            } else {
                return {
                    title: 'Logout',
                    drawerIcon: ({tintColor}) => (
                        <Icon name="md-power"/>
                    )
                }
            }
        }
    }
},
{
    drawerPosition: 'left',
    contentComponent: CustomDrawerNavigation,
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    drawerToggleRoute: 'DrawerToggle',
    drawerWidth: (width / 3) * 2,
});
export default Drawer;