import React, { Component, createContext } from 'react';
import {createStackNavigator} from 'react-navigation-stack';
import MySurveyList from '../pages/MySurveyList';
import ProfileEdit from '../pages/ProfileEdit';



const ProfileStackNavigator = createStackNavigator({

    ModificaProfilo: {
        screen: ProfileEdit,
        navigationOptions: {
            title: 'Modifica profilo',
            drawerIcon: ({ tintColor }) => (
                <Icon name = "md-person" />
            )
        }
    },

}, {headerMode: 'none'});

export default ProfileStackNavigator;