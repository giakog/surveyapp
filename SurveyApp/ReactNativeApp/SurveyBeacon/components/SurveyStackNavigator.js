import React, { Component, createContext } from 'react';
import {createStackNavigator} from 'react-navigation-stack';
import SurveyList from "../pages/SurveyList";
import SurveyDetail from "../pages/SurveyDetail";
import SurveyDeleteBlock from "../pages/SurveyDeleteBlock";
import SurveyActivate from "../pages/SurveyActivate";
import SurveyEdit from "../pages/SurveyEdit";
import QuestionCreate from "../pages/QuestionCreate";
import QuestionEdit from "../pages/QuestionEdit";
import QuestionDelete from "../pages/QuestionDelete";
import OptionCreate from "../pages/OptionCreate";
import OptionDelete from "../pages/OptionDelete";
import SubmissionStart from "../pages/SubmissionStart";
import MySurveyList from "../pages/MySurveyList";
import {Icon} from "native-base";
import SurveyStats from "../pages/SurveyStats";
import OptionEdit from "../pages/OptionEdit";
import SurveyThanks from "../pages/SurveyThanks";

const SurveyStackNavigator = createStackNavigator({
    MySurveyList: {
         screen: MySurveyList,
        navigationOptions: {
             title: 'Miei Sondaggi'
        }
     },
    ListaSondaggi: {
        screen: SurveyList,
        navigationOptions: {
        title: 'Lista Sondaggi'
        }
    },
    DettagliSondaggio: {
        screen: SurveyDetail,
        navigationOptions: {
            title: 'Dettagli Sondaggio'
        }
    },
    EliminaInterrompiSondaggio: {
        screen: SurveyDeleteBlock,
        navigationOptions: {
            title: 'Elimina Interrompi Sondaggio'
        }
    },
    ModificaSondaggio: {
        screen: SurveyEdit,
        navigationOptions: {
            title: 'Modifica Sondaggio'
        }
    },
    AttivaSondaggio: {
        screen: SurveyActivate,
        navigationOptions: {
            title: 'Attiva Sondaggio'
        }
    },
    CreaDomanda: {
        screen: QuestionCreate,
        navigationOptions: {
            title: 'Crea Domanda'
        }
    },
    ModificaDomanda: {
        screen: QuestionEdit,
        navigationOptions: {
            title: 'Modifica Domanda'
        }
    },
    EliminaDomanda: {
        screen: QuestionDelete,
        navigationOptions: {
            title: 'Elimina Domanda'
        }
    },
    CreaOpzioni: {
        screen: OptionCreate,
        navigationOptions: {
            title: 'Aggiungi Opzioni'
        }
    },
    ModificaOpzione: {
        screen: OptionEdit,
        navigationOptions: {
            title: 'Modifica Opzione'
        }
    },
    EliminaOpzione: {
        screen: OptionDelete,
        navigationOptions: {
            title: 'Elimina Opzione'
        }
    },
    IniziaSondaggio: {
        screen:SubmissionStart,
        navigationOptions: {
            title: 'Inizia Sondaggio'
        }
    },
    RingraziamentoSondaggio: {
        screen:SurveyThanks,
        navigationOptions: {
            title: 'Ringraziamento'
        }
    },
    StatisticheSondaggio: {
        screen:SurveyStats,
        navigationOptions: {
            title: 'Statistiche Sondaggio'
        }
    }
}, {headerMode: 'none'});

export default SurveyStackNavigator;