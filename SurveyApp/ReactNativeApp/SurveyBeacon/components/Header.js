import React from 'react';
import { Header } from 'react-native-elements';
import { Icon } from 'native-base';

const CustomHeader = props => {
    return (
        <Header
            leftComponent={<Icon name="menu" style={{color: '#e1e1e1'}} onPress={() => props.parent.navigation.openDrawer()} />}
            centerComponent={{ text: 'SurveyBeacon', style: { color: '#e1e1e1', fontSize: 18 , marginTop:5} }}
            containerStyle={{
                backgroundColor: '#4169e1',
                justifyContent: 'space-around',
            }}
            rightComponent={<Icon name="home" style={{color: '#e1e1e1'}} onPress={() => props.parent.navigation.navigate('CategoryList')} />}
        />
    );
};

export default CustomHeader;