import React, { Component } from 'react';
import {View, Text, StyleSheet, Button, Image, ActivityIndicator, FlatList, TextInput} from 'react-native';
import CustomHeader from '../components/Header';
import Card from '../components/Card';
import { TouchableOpacity, TouchableWithoutFeedback, ScrollView } from 'react-native-gesture-handler';

import { Dimensions } from 'react-native';
import { IconButton } from 'react-native-paper';
import {Picker} from "native-base";
import MultiSelect from "react-native-multiple-select";

const {width, height} = Dimensions.get('window');



class SurveyEdit extends Component {
    constructor(props){
        super(props);
        this.state ={
            isLoading: true,
            id_sondaggio: this.props.navigation.state.params.id_sondaggio,
            error_msg: "",
            title: "",
            description: "",
            thanks_message: "",
            category: -1,
            selectedItems: [],
        }
    }

    componentDidMount() {

        this.fetchDettagliSondaggio();
        this.willFocusSubscription = this.props.navigation.addListener(
          'willFocus',
          () => {
            this.setState({
                isLoading: true,
            }, function(){

            });
            this.fetchDettagliSondaggio();
          }
        );
    }

    componentWillUnmount() {
        this.willFocusSubscription.remove();
    }

    fetchDettagliSondaggio() {
        let tags = [];
        return fetch('http:/192.168.1.34:8000/api/survey/detail/' +
            this.props.navigation.state.params.id_sondaggio + '/creator', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Token ' + global.user_key,
            }
        })
            .then((response) => response.json())
            .then((responseJson) => {
                for (let i = 0; i < responseJson.tags.length; ++i) {
                    tags.push(responseJson.tags[i].id);
                }
                this.setState({
                    isLoading: false,
                    title: responseJson.title,
                    description: responseJson.description,
                    thanks_message: responseJson.thanks_message,
                    category: responseJson.category,
                    selectedItems: tags
                }, function () {

                });
            })
            .catch((error) => {
                console.log("Errore")
            });
    }
    modificaSondaggio = () => {
        console.log(this.state.category.id, this.state.title, this.state.tags, this.state.thanks_message, this.state.description);
        if (this.state.title != "" && this.state.description != -1 && this.state.category != ""
            && this.state.thanks_message != "" && this.state.selectedItems.length > 0) {
            fetch('http:/192.168.1.34:8000/api/survey/'+ this.props.navigation.state.params.id_sondaggio + '/edit',
            {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Token ' + global.user_key,
                },
              body: JSON.stringify({
                      title: this.state.title,
                      description: this.state.description,
                      thanks_message: this.state.thanks_message,
                      category: this.state.category.id,
                      tags: this.state.selectedItems,
                      status: 0,
              }),
            })
                .then(res => res.json())
                .then((res) => {
                    if (res.id != null) {
                        this.clearFields();
                        this.props.navigation.goBack(null);
                    } else {
                        this.setState({error_message: "Errore: controlla i campi inseriti e riprova."});
                    }
                })
                .catch((error) => {
                    console.log("Errore")
                    this.creaSondaggio;
                })
        } else {
            this.setState({error_message: "Errore: assicurati di riempire tutti i campi."});
        }
    };

    clearFields = () => {
        this.setState({error_message: "",
        title: "",
        description: "",
        thanks_message: "",
        category: "",
        selectedItems: [],
        });

        this.txtTitolo.clear();
        this.txtDescrizione.clear();
        this.txtThanks_message.clear();
    };
    onSelectedItemsChange = selectedItems => {
        this.setState({ selectedItems });
        console.log(selectedItems);
    };

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: height / 2}}>
                    <ActivityIndicator/>
                </View>
            )
        }
        const { selectedItems } = this.state;
        return (
            <View style={styles.screen}>
                <CustomHeader parent={this.props} />

                <View style={styles.contentbar}>
                    <View style={styles.leftcontainer}>
                        <IconButton icon="arrow-left" onPress={() => this.props.navigation.goBack(null)} />
                    </View>
                    <Text style={styles.title}>
                        Modifica sondaggio
                    </Text>
                    <View style={styles.rightcontainer}></View>
                </View>

                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{alignItems: 'center'}}>
                        <Card style={styles.inputContainer}>
                            <View style={styles.data}>
                                <View style={{flexDirection: 'row'}}>
                                    <View>
                                        <View style={styles.entryTitle}>
                                            <Text style={styles.textTitle}>Titolo:</Text>
                                            <Text style={styles.asteriskStyle}>*</Text>
                                        </View>
                                        <View style={styles.entryTitle}>
                                            <Text style={styles.textTitle}>Descrizione: </Text>
                                            <Text style={styles.asteriskStyle}>*</Text>
                                        </View>
                                        <View style={styles.entryTitle}>
                                            <Text style={styles.textTitle}>Ringraziamento: </Text>
                                            <Text style={styles.asteriskStyle}>*</Text>
                                        </View>
                                        <View style={styles.entryTitle}>
                                            <Text style={styles.textTitle}>Cateogria: </Text>
                                            <Text style={styles.asteriskStyle}>*</Text>
                                        </View>
                                        <View style={styles.entryTitle}>
                                            <Text style={styles.textTitle}>Tags: </Text>
                                            <Text style={styles.asteriskStyle}>*</Text>
                                        </View>

                                    </View>

                                    <View>
                                        <View style={styles.textContainer}>
                                            <TextInput editable maxLength={95}
                                                       value={this.state.title}
                                            ref={input => { this.txtTitolo = input }}
                                            onChangeText={(value) => this.setState({title: value})}/>
                                        </View>
                                        <View style={styles.textContainer}>
                                            <TextInput editable maxLength={245}
                                                       value={this.state.description}
                                            ref={input => { this.txtDescrizione = input }}
                                            onChangeText={(value) => this.setState({description: value})}/>
                                        </View>
                                        <View style={styles.textContainer}>
                                            <TextInput editable maxLength={245} multiline={true}
                                                       value={this.state.thanks_message}
                                            ref={input => { this.txtThanks_message = input }}
                                            onChangeText={(value) => this.setState({thanks_message: value})} />
                                        </View>
                                        <View>
                                        <Picker
                                            style={styles.picker} itemStyle={styles.pickerItem}
                                            selectedValue={this.state.category.id}
                                            onValueChange={(itemValue) => this.setState({category: itemValue})}
                                            >
                                            { global.category_list.map((item, key) =>
                                                <Picker.Item label={item.title} value={item.id} key={key} />
                                            )}
                                        </Picker>
                                        </View>
                                        <View style={{flex: 1}}>
                                        <MultiSelect
                                            hideTags
                                            items={global.tag_list}
                                            uniqueKey="id"
                                            onSelectedItemsChange={this.onSelectedItemsChange}
                                            selectedItems={selectedItems}
                                            selectText="Scegli tag"
                                            searchInputPlaceholderText="Cerca tag..."
                                            onChangeInput={ (text)=> console.log(text)}
                                            tagRemoveIconColor="#CCC"
                                            tagBorderColor="#CCC"
                                            tagTextColor="#CCC"
                                            selectedItemTextColor="#CCC"
                                            selectedItemIconColor="#CCC"
                                            itemTextColor="#000"
                                            displayKey="title"
                                            searchInputStyle={{ color: '#CCC' }}
                                            submitButtonColor="#CCC"
                                            submitButtonText="conferma"
                                            />
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.controlli}>
                                    <View style={styles.buttonview}>
                                        <Button title="Conferma modifica" onPress={() => {
                                            this.modificaSondaggio();}} />
                                    </View>
                                </View>

                                <View style={{paddingTop: 10}}></View>
                                <Text style={{color: 'red'}}>{this.state.error_message}</Text>
                                <View style={{paddingTop: 10}}></View>

                                <View style={{flexDirection: 'row', marginTop: 20, marginBottom: 5}}>
                                    <Text>I campi contrassegnati con</Text>
                                    <Text style={styles.asteriskStyle}>*</Text>
                                    <Text>sono obbligatori.</Text>

                                </View>
                            </View>
                        </Card>
                    </View>
                </ScrollView>
            </View>
        );
    }

}
const styles = StyleSheet.create({
    screen: {
        flex: 1
    },
    title: {
        fontSize: 20,
        marginVertical: 10
    },
    buttonview: {
        width: 150,
        paddingRight: 5,
        paddingLeft: 5
    },
    inputContainer: {
        minWidth: '96%'
    },
    controlli: {
        paddingTop: 20,
        paddingRight: 5,
        alignItems: 'center'
    },
    data: {
        paddingTop: 20,
        paddingLeft: 10
    },
    entryTitle: {
        marginBottom: 5,
        marginTop: 9,
        flexDirection: 'row'
    },
    textTitle: {
        fontWeight: 'bold'
    },
    contentbar: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
      },
    leftcontainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    rightcontainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    textContainer: {
        borderWidth: 1,
        height: 28,
        width: width - width / 2,
        marginLeft: 10,
        marginBottom: 3,
        marginTop: 3
    },
    caricaStyle: {
        marginBottom: 3,
        marginTop: 3,
        height: 28,
        width: 100,
        marginLeft: 10,
        borderWidth: 1,
        alignItems: 'center'
    },
    checkBoxStyle: {
        marginLeft: 10,
        marginBottom: 6,
        marginTop: 3
    },
    asteriskStyle: {
        marginLeft: 3,
        marginRight: 3,
        color: 'red'
    },
    picker: {
        marginLeft: 10,
        width: width - width / 2,
        height: 28,
        backgroundColor: '#e7e7e7',
        marginBottom: 3,
        marginTop: 3
    },
    pickerItem: {
        color: 'white'
    }
});

export default SurveyEdit;