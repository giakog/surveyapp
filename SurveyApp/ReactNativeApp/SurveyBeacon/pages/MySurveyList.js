import React, { Component } from 'react';
import {View, Text, StyleSheet, Button, Image, Dimensions, ActivityIndicator, FlatList, TextInput} from 'react-native';
import CustomHeader from '../components/Header';
import Card from '../components/Card';
import { TouchableOpacity, TouchableWithoutFeedback, ScrollView } from 'react-native-gesture-handler';
import {Picker} from 'native-base';
import { IconButton, ThemeProvider } from 'react-native-paper';

const {width, height} = Dimensions.get('window');


class MySurveyList extends Component {

    constructor(props){
        super(props);
        this.state ={
            error_msg: "",
            isLoading: true,
            filter: "0",
            tag: "0",
            order: "0",
            txt: ""
        }
    }
    ShowHidePickers = () => {
        if (this.state.show_pickers == true) {
          this.setState({ show_pickers: false });
        } else {
          this.setState({ show_pickers: true });
        }
    };


    componentDidMount() {
        this.fetchSurveyList();
        this.willFocusSubscription = this.props.navigation.addListener(
          'willFocus',
          () => {
            this.setState({
                isLoading: true,
            }, function(){

            });
            this.fetchSurveyList();
          }
        );
    }

    componentWillUnmount() {
    this.willFocusSubscription.remove();
    }

    fetchSurveyList() {
        fetch('http://192.168.1.34:8000/api/survey/list/my',
        {
          method: 'GET',
          headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Token ' + global.user_key,
          }
        })
        .then(response => response.json())
        .then((responseJson) => {
            this.setState({
                isLoading: false,
                dataSource: responseJson,
            }, function(){

            });
        })
        .catch((error) => {
            this.setState({error_msg: "Errore caricamento sondaggi"});
        })
    }

    fetchFilteredMySurveyList() {

        if(this.state.filter == "submissions"|| this.state.filter == "date") {
            fetch('http://192.168.1.34:8000/api/survey/list/my/sorted-by/' + this.state.filter  + '/order/' + this.state.order,
                {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Token ' + global.user_key,
                    }
                })
                .then(response => response.json())
                .then((responseJson) => {
                    this.setState({
                        isLoading: false,
                        dataSource: responseJson,
                    }, function () {

                    });
                })
                .catch((error) => {
                    this.setState({error_message: "Errore: Nessun sondaggio trovato"});
                })
        }
        else if (this.state.filter == "tag") {
            fetch('http://192.168.1.34:8000/api/survey/list/my/filtered-by/' + this.state.tag,
                {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Token ' + global.user_key,
                    }
                })
                .then(response => response.json())
                .then((responseJson) => {
                    this.setState({
                        isLoading: false,
                        dataSource: responseJson,
                    }, function () {

                    });
                })
                .catch((error) => {
                    this.setState({error_message: "Errore: Nessun sondaggio trovato"});
                })
        }
        else if(this.state.filter == "cerca") {
            fetch('http://192.168.1.34:8000/api/survey/list/my/search/?q=' + this.state.txt,
                {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Token ' + global.user_key,
                    }
                })
                .then(response => response.json())
                .then((responseJson) => {
                    this.setState({
                        dataSource: responseJson,
                    }, function () {

                    });
                })
                .catch((error) => {
                    this.setState({error_message: "Errore: Nessun sondaggio trovato"});
                })
        }
    }

    render() {

        if(this.state.isLoading){
            return(
                <View style={{flex: 1, paddingTop: height / 2}}>
                    <ActivityIndicator/>
                </View>
            )
        }
        console.log(global.user_key);
        return (

            <View style={styles.screen}>

                <View style={{alignSelf: 'flex-start', width: '100%', alignItems: 'center'}}>
                    <CustomHeader parent={this.props} />

                    <View style={styles.contentbar}>
                        <View style={styles.leftcontainer}></View>
                        <Text style={styles.title}>
                            Miei Sondaggi
                        </Text>
                        <View style={styles.rightcontainer}>
                            <IconButton icon="filter" style={{paddingRight: 10}} onPress={this.ShowHidePickers} />
                        </View>
                    </View>

                    {this.state.show_pickers ? (
                        <View style={styles.pickers}>
                             <Picker
                                style={styles.picker} itemStyle={styles.pickerItem}
                                selectedValue={this.state.filter}
                                onValueChange={(itemValue) => { this.setState({filter: itemValue})}}
                                >
                                <Picker.Item label="Filtro" value="0" />
                                <Picker.Item label="Sottoscrizioni" value="submissions" />
                                <Picker.Item label="Tag" value="tag" />
                                <Picker.Item label="Data" value="date" />
                                <Picker.Item label="Cerca.." value="cerca" />
                            </Picker>
                            <View style={{paddingBottom: 5}}></View>
                            {this.state.filter == "submissions" ? (
                                <Picker
                                    style={styles.picker} itemStyle={styles.pickerItem}
                                    selectedValue={this.state.order}
                                    onValueChange={(itemValue) => {this.setState({order: itemValue})}}
                                    >
                                    <Picker.Item label="Più popolari" value="0" />
                                    <Picker.Item label="Meno popolari" value="1" />
                                </Picker>

                            ): null}
                            <View style={{paddingBottom: 5}}></View>
                            {this.state.filter == "tag" ? (
                                <Picker
                                    style={styles.picker} itemStyle={styles.pickerItem}
                                    selectedValue={this.state.tag}
                                    onValueChange={(itemValue) => {this.setState({tag: itemValue})}}
                                    >
                                    <Picker.Item label="-" value="0" />
                                    { global.tag_list.map((item, key) =>
                                        <Picker.Item label={item.title} value={item.id} key={key} />
                                )}
                                </Picker>
                            ): null}
                            <View style={{paddingBottom: 5}}></View>
                            {this.state.filter == "date" ? (
                                <Picker
                                    style={styles.picker} itemStyle={styles.pickerItem}
                                    selectedValue={this.state.order}
                                    onValueChange={(itemValue) => {this.setState({order: itemValue})}}
                                    >
                                    <Picker.Item label="Più recenti" value="0" />
                                    <Picker.Item label="Meno recenti" value="1" />
                                </Picker>
                            ): null}

                            <View style={{paddingBottom: 5}}></View>
                            {this.state.filter == "cerca" ? (
                            <TextInput
                                placeholder="Titolo"
                                style={{fontSize:16, paddingLeft:15}}
                                editable maxLength={245}
                                value={this.state.txt}
                                ref={input => { this.txtText = input }}
                                onChangeText={(value) => this.setState({txt: value})}/>
                            ): null}
                            <View style={{paddingBottom: 5}}></View>
                            {this.state.filter != 0 ? (
                            <View style={styles.buttonview}>
                                <Button title="Filtra Sonsaggi" onPress={() => this.fetchFilteredMySurveyList()}/>
                            </View>
                            ): null}


                            {global.logged_in ? (
                                <View style={{paddingTop: 6}}></View>
                            ) : <View style={{paddingTop: 2}}></View>}

                        </View>
                    ) : null}
                </View>
                <View style={styles.flatlistview}>
                    <FlatList
                        style={{flex: 1}}
                        data={this.state.dataSource}
                        renderItem={({item, index}) =>
                        <TouchableOpacity style={styles.touchableopacity} activeOpacity={.8} onPress={() =>
                            this.props.navigation.navigate('DettagliSondaggio', {id_sondaggio: item.id, id_creator:item.creator})}>
                            <Card style={styles.inputContainer}>

                                <View style={styles.data}>
                                    <Text style={styles.sondaggioTitle} numberOfLines={1}>{item.title}</Text>
                                    <Text style={styles.sondaggioSubtitle} numberOfLines={2}>{item.description}</Text>
                                    <View style={styles.textInline}>
                                        <Text style={{fontWeight: 'bold', fontStyle: 'italic'}}>Categoria: </Text>
                                        <Text>{item.category.title}</Text>
                                    </View>
                                    <View style={styles.textInline}>
                                        <Text style={{fontWeight: 'bold', fontStyle: 'italic'}}>Tag: </Text>
                                        {item.tags.length > 1 ?(
                                        <Text>{item.tags[0].title} {item.tags[1].title}</Text>
                                            ): null}
                                        {item.tags.length == 1 ?(
                                            <Text>{item.tags[0].title}</Text>
                                            ): null}
                                    </View>
                                    <View style={styles.textInline}>
                                        <Text style={{fontWeight: 'bold', fontStyle: 'italic'}}>Stato: </Text>
                                        {item.status == 0 ?(
                                        <Text style={{width: width - width / 2.6, color:'orange'}} numberOfLines={2}>Inattivo</Text>
                                            ): null}
                                            {item.status == 1 ?(
                                        <Text style={{width: width - width / 2.6, color:'green'}} numberOfLines={2}>Attivo</Text>
                                            ): null}
                                            {item.status == 2 ?(
                                        <Text style={{width: width - width / 2.6, color:'red'}} numberOfLines={2}>Bloccato</Text>
                                            ): null}
                                    </View>
                                    <View style={styles.textInline}>
                                        <Text style={{fontWeight: 'bold', fontStyle: 'italic'}}>Numero sottoscrizioni: </Text>
                                        <Text>{item.sub_completed.length}</Text>
                                    </View>
                                </View>
                            </Card>
                        </TouchableOpacity>
                        }
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    screen: {
        flex: 1,
        alignItems: 'center'
    },
    sondaggioLogo: {
        width: 80,
        height: 80
    },
    title: {
        fontSize: 20,
        marginVertical: 10
    },
    inputContainer: {
        flex: 1,
        minWidth: '96%',
        flexDirection: 'row'
    },
    sondaggioTitle: {
        fontSize: 18,
        fontWeight: 'bold',
        width: width - width / 2.6
    },
    sondaggioSubtitle: {
        width: width - width / 2.6
    },
    data: {
        flex: 1
    },
    image: {
        paddingRight: 10
    },
    textInline: {
        flexDirection: 'row'
    },
    touchableopacity: {
        flex: 1,
        alignItems: 'center'
    },
    contentbar: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    leftcontainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    rightcontainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    flatlistview: {
        flex: 1,
        width: '100%',
        justifyContent: 'space-between',
        alignItems: 'center'
    }
});

export default MySurveyList;