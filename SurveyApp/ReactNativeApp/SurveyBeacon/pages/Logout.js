import React, { Component } from 'react';

import { Dimensions } from 'react-native';

const {width, height} = Dimensions.get('window');

class Logout extends Component {

    componentDidMount() {
        this.logoutExecute();
        this.willFocusSubscription = this.props.navigation.addListener(
          'willFocus',
          () => {
            this.logoutExecute();
          }
        );
    }

    componentWillUnmount() {
    this.willFocusSubscription.remove();
    }

    logoutExecute() {
        global.logged_in = false;
        global.user_key = "-1";
        global.username = "";
        global.user_id = -1;
        global.cs = "";
        global.ed = "";
        global.gender = "";
        global.ws = "";
        global.country = "";
        global.city = "";
        this.props.navigation.goBack(null);
    }

    render() {
        return null;
    }
}

export default Logout;