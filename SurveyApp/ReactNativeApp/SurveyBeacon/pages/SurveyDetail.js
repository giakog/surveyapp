import React, { Component } from 'react';
import {View, Text, StyleSheet, Button, Image, ActivityIndicator, FlatList, LogBox, TextInput} from 'react-native';
import {Picker} from "native-base";
import CustomHeader from '../components/Header';
import Card from '../components/Card';
import { TouchableOpacity, TouchableWithoutFeedback, ScrollView } from 'react-native-gesture-handler';

import { Dimensions } from 'react-native';
import { IconButton } from 'react-native-paper';
import CountryPicker from "react-native-country-picker-modal";

const {width, height} = Dimensions.get('window');

let id_sondaggio;
let id_creator;
let questions_data;

class SurveyDetail extends Component {

    constructor(props){
        super(props);
        this.state ={
            isLoading: true,
            gender: "0",
            cs: "0",
            ed: "0",
            ws: "0",
            city: "",
            country: "IT",
            pick_country: "0",
            question: "0",
            choice: "0",
            show_picker: true,
            submission_exist: false,
            creator: false,
            id_sondaggio: this.props.navigation.state.params.id_sondaggio,
            id_creator: this.props.navigation.state.params.id_creator
        }
    }

    componentDidMount() {
        this.fetchDettagliSondaggio().then(r => {
            if (global.logged_in && !this.state.creator) {
                this.fetchSottomissioneUtente();
            }
        });

        this.willFocusSubscription = this.props.navigation.addListener(
          'willFocus',
          () => {
            this.setState({
                isLoading: true,
            }, function(){

            });
            this.fetchDettagliSondaggio().then(r => {
                if (global.logged_in && !this.state.creator) {
                    this.fetchSottomissioneUtente();
                }
            });
          }
        );
    }

    componentWillUnmount() {
        this.willFocusSubscription.remove();
    }

    ShowHidePickers = () => {
        if (this.state.show_pickers == true) {
          this.setState({ show_pickers: false });
        } else {
          this.setState({ show_pickers: true });
        }
    };

    fetchDettagliSondaggio() {
        if (global.logged_in && this.state.id_creator === global.user_id) {
            return fetch('http:/192.168.1.34:8000/api/survey/detail/' +
                this.props.navigation.state.params.id_sondaggio + '/creator',{
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Token ' + global.user_key,
                },
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    this.setState({
                        isLoading: false,
                        id_sondaggio: this.props.navigation.state.params.id_sondaggio,
                        creator: true,
                        dataSource: responseJson,
                        question_list: responseJson.questions
                    }, function () {

                    });

                })
                .catch((error) => {
                    console.log("Errore");
                });
        }
        else {
            return fetch('http:/192.168.1.34:8000/api/survey/detail/' +
                this.props.navigation.state.params.id_sondaggio + '/submitter')
                .then((response) => response.json())
                .then((responseJson) => {
                    this.setState({
                        isLoading: false,
                        id_sondaggio: this.props.navigation.state.params.id_sondaggio,
                        dataSource: responseJson
                    }, function () {

                    });

                })
                .catch((error) => {
                    console.log("Errore");
                });

        }
    }

    fetchSottomissioneUtente() {
        return fetch('http:/192.168.1.34:8000/api/survey/' +
            this.props.navigation.state.params.id_sondaggio + '/my', {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Token ' + global.user_key,
            },
        })
            .then((response) => response.json())
            .then((responsejson) => {
                this.setState( {
                    submission_exist: true,
                    dataSource2: responsejson
                }, function () {

                });
            })
    }
    getIndex(question) {
        return this.state.dataSource2.answers.findIndex(obj => obj.question === question);
    }
    getIndex2(question) {
        console.log(this.state.dataSource.questions.findIndex(obj => obj.id == question))
        return this.state.dataSource.questions.findIndex(obj => obj.id == question);
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: height / 2}}>
                    <ActivityIndicator/>
                </View>
            )
        }
        LogBox.ignoreAllLogs();
        data = this.state.dataSource;

        //console.log("data"+data)
        let sub = this.state.dataSource2;

        let usr = [];
        if (this.state.creator) {
            for (let i = 0; i < data.submissions.length; ++i) {
                usr = usr.concat(data.submissions[i].user);
            }
        }

        return (
            <View style={styles.screen}>
                <View style={{alignSelf: 'flex-start', width: '100%', alignItems: 'center'}}>
                <CustomHeader parent={this.props} />

                <View style={styles.contentbar}>
                    <View style={styles.leftcontainer}>
                        <IconButton icon="arrow-left" onPress={() => this.props.navigation.goBack(null)} />
                    </View>
                    <Text style={styles.title}>
                        {data.title}
                    </Text>
                    <View style={styles.rightcontainer}>
                        {global.logged_in && this.state.creator && data.status != 0 ?(
                            <IconButton icon="filter" style={{paddingRight: 10}} onPress={this.ShowHidePickers} />
                        ): null}
                    </View>
                </View>

                    {this.state.show_pickers ? (
                        <View style={styles.pickers}>
                            <Picker
                                style={styles.picker} itemStyle={styles.pickerItem}
                                selectedValue={this.state.question}
                                onValueChange={(itemValue) => { this.setState({question: itemValue, choice:"0"})}}
                                >
                                <Picker.Item label="Domanda" value="0" />
                                { data.questions.map((item, key) =>
                                        <Picker.Item label={item.text} value={item.id} key={key} />
                                )}
                            </Picker>
                            <View style={{paddingBottom: 5}}></View>
                            {this.state.question != "0" ?(
                            <Picker
                                style={styles.picker} itemStyle={styles.pickerItem}
                                selectedValue={this.state.choice}
                                onValueChange={(itemValue) => { this.setState({choice: itemValue})}}
                                >
                                <Picker.Item label="Opzione" value="0" />
                                { data.questions[this.getIndex2(this.state.question)].choices.map((item, key) =>
                                        <Picker.Item label={item.text} value={item.id} key={key} />
                                )}
                            </Picker>
                            ): null}
                            <View style={{paddingBottom: 5}}></View>
                            <Picker
                                style={styles.picker} itemStyle={styles.pickerItem}
                                selectedValue={this.state.gender}
                                onValueChange={(itemValue) => { this.setState({gender: itemValue})}}
                                >
                                <Picker.Item label="Genere" value="0" />
                                <Picker.Item label="Tutti" value="X" />
                                <Picker.Item label="Maschio" value="maschio" />
                                <Picker.Item label="Femmina" value="femmina" />
                                <Picker.Item label="Altro" value="altro" />
                            </Picker>

                            <View style={{paddingBottom: 5}}></View>

                            <Picker
                                style={styles.picker} itemStyle={styles.pickerItem}
                                selectedValue={this.state.cs}
                                onValueChange={(itemValue) => { this.setState({cs: itemValue})}}
                                >
                                <Picker.Item label="Stato civile" value="0" />
                                <Picker.Item label="Tutti" value="X" />
                                <Picker.Item label="Single" value="single" />
                                <Picker.Item label="Sposato/a" value="sposato/a" />
                                <Picker.Item label="Divorziato/a" value="divorziato/a" />
                                <Picker.Item label="Vedovo/a" value="vedovo/a" />
                            </Picker>
                            <View style={{paddingBottom: 5}}></View>
                            <Picker
                                style={styles.picker} itemStyle={styles.pickerItem}
                                selectedValue={this.state.ed}
                                onValueChange={(itemValue) => {this.state.ed = itemValue}}
                                >
                                <Picker.Item label="Educazione" value="0" />
                                <Picker.Item label="Tutti" value="X" />
                                <Picker.Item label="Licenza Elementare" value="elementare" />
                                <Picker.Item label="Licenza Media " value="media" />
                                <Picker.Item label="Diploma" value="diploma" />
                                <Picker.Item label="Laurea" value="laurea" />
                            </Picker>
                            <View style={{paddingBottom: 5}}></View>
                            <Picker
                                style={styles.picker} itemStyle={styles.pickerItem}
                                selectedValue={this.state.ws}
                                onValueChange={(itemValue) => { this.setState({ws: itemValue})}}
                                >
                                <Picker.Item label="Stato Lavorativo" value="0" />
                                <Picker.Item label="Tutti" value="X" />
                                <Picker.Item label="Studente" value="studente" />
                                <Picker.Item label="Disoccupato" value="disoccupato" />
                                <Picker.Item label="Occupato" value="occupato" />
                                <Picker.Item label="Ritirato" value="ritirato" />
                            </Picker>
                            <View style={{paddingBottom: 5}}></View>
                            <Picker
                                style={styles.picker} itemStyle={styles.pickerItem}
                                selectedValue={this.state.pick_country}
                                onValueChange={(itemValue) => { this.setState({pick_country: itemValue})}}
                                >
                                <Picker.Item label="Paese" value="0" />
                                <Picker.Item label="Tutti" value="X" />
                                <Picker.Item label="Scegli" value="1" />
                            </Picker>
                            <View style={{paddingBottom: 5}}></View>
                            {this.state.pick_country == 1 ?(
                            <CountryPicker
                                withFilter={true}
                                withCountryNameButton={true}
                                countryCode={this.state.country}
                                onSelect={(value)=> this.setState({country: value.cca2})}
                                cca2={this.state.country}
                                translation='ita'
                            />
                            ): null}
                            <View style={{paddingBottom: 5}}></View>
                            <TextInput
                                placeholder="Citta"
                                style={{fontSize:16, paddingLeft:15}}
                                editable maxLength={245}
                                value={this.state.city}
                                ref={input => { this.txtText = input }}
                                onChangeText={(value) => this.setState({city: value})}/>
                            <View style={{paddingBottom: 5}}></View>
                            <View style={styles.buttonview}>
                                <Button title="Mostra Statistiche" onPress={() => this.props.navigation.navigate('StatisticheSondaggio',
                                    {question:this.state.question, choice:this.state.choice, cs: this.state.cs, gender: this.state.gender,
                                        ed: this.state.ed, ws: this.state.ws, pick_country:this.state.pick_country,
                                        country:this.state.country, city:this.state.city, id_sondaggio:this.state.id_sondaggio})}/>
                            </View>
                            {global.logged_in ? (
                                <View style={{paddingTop: 6}}></View>
                            ) : <View style={{paddingTop: 2}}></View>}

                        </View>
                    ) : null}
                </View>

                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{alignItems: 'center'}}>
                        <Card style={styles.inputContainer}>
                            <View style={styles.data}>
                                <View style={styles.entry}>
                                    <Text style={styles.textTitle}>Descrizione: </Text>
                                    <Text style={styles.textData}>{data.description}</Text>
                                </View>

                                <View style={styles.entry}>
                                    <Text style={styles.textTitle}>Pubblicato da: </Text>
                                    <Text style={styles.textData}>{data.creator_user.username}</Text>
                                </View>
                                { global.logged_in == true && this.state.creator ? (
                                <View style={styles.entry}>
                                    <Text style={styles.textTitle}>Sottomissioni: </Text>
                                    <Text style={styles.textData}>{data.sub_completed.length}</Text>
                                </View>
                                ): null }

                                <View style={{alignItems: 'center'}}>
                                    <View style={styles.controlli}>
                                        { global.logged_in == true && this.state.creator  && data.status == 0 ? (
                                        <View style={styles.buttonview}>
                                            <Button title="Attiva" onPress={() => this.props.navigation.navigate('AttivaSondaggio', {data_sondaggio: this.state.dataSource, id_sondaggio: this.state.id_sondaggio})}/>
                                            <Button title="Modifica" onPress={() => this.props.navigation.navigate('ModificaSondaggio', {id_sondaggio: this.state.id_sondaggio})}/>
                                            <Button title="Elimina" onPress={() => this.props.navigation.navigate('EliminaInterrompiSondaggio', {id_op: 0, data_sondaggio: this.state.dataSource, id_sondaggio: this.state.id_sondaggio})}/>
                                        </View>
                                        ) : null }
                                        { global.logged_in == true && this.state.creator && data.status == 1 ? (
                                            <View style={styles.buttonview}>
                                                <Button title="Elimina" onPress={() => this.props.navigation.navigate('EliminaInterrompiSondaggio', {id_op: 0, data_sondaggio:data, id_sondaggio: this.state.id_sondaggio})}/>
                                                <Button title="Interrompi" onPress={() => this.props.navigation.navigate('EliminaInterrompiSondaggio', {id_op: 1, data_sondaggio:data, id_sondaggio: this.state.id_sondaggio})}/>
                                            </View>

                                        ) : null }
                                        { global.logged_in == true && this.state.creator && data.status == 2 ? (
                                            <View style={styles.buttonview}>
                                                <Button title="Elimina" onPress={() => this.props.navigation.navigate('EliminaInterrompiSondaggio', {id_op: 0, data_sondaggio:data, id_sondaggio: this.state.id_sondaggio})}/>
                                            </View>
                                        ) : null }
                                        { global.logged_in == true && !this.state.creator &&  data.status == 1 && !this.state.submission_exist ? (
                                        <View style={{flexDirection: 'row'}}>
                                            <View style={styles.buttonview}>
                                                <Button title="Inizia" onPress={() => this.props.navigation.navigate('IniziaSondaggio', {id_sondaggio: this.state.id_sondaggio, id_creator: this.state.id_creator})}/>
                                            </View>
                                        </View>
                                        ) : null }
                                    </View>
                                </View>
                                <FlatList
                                    data={this.state.dataSource.questions}
                                    extraData={this.state.dataSource.submissions}
                                    style={{flex:1}}
                                    renderItem={({ item, index }) =>
                                        <View style={{marginTop:20}}>
                                            { global.logged_in == false || !this.state.creator  || data.status != 0 ? (
                                                <Text style={styles.textTitle}>{index + 1}) {item.text}</Text>
                                            ) : null }
                                            { global.logged_in == true && this.state.creator  && data.status == 0 ? (
                                            <View style={styles.entry2}>
                                                <Text style={styles.textTitle}>{index + 1}) {item.text}</Text>
                                                <TouchableOpacity onPress={() => this.props.navigation.navigate('CreaOpzioni', {id_sondaggio: this.state.id_sondaggio, id_domanda: item.id})}>
                                                    {item.type != 1 ?(
                                                    <Text style={styles.btnText}>Aggiungi Opzioni</Text>
                                                    ): null}
                                                </TouchableOpacity>
                                                <TouchableOpacity onPress={() => this.props.navigation.navigate('ModificaDomanda', {id_sondaggio: this.state.id_sondaggio, id_domanda: item.id})}>
                                                    <Text style={styles.btnText}>Modifica</Text>
                                                </TouchableOpacity>
                                                <TouchableOpacity onPress={() => this.props.navigation.navigate('EliminaDomanda', {id_sondaggio: this.state.id_sondaggio, id_domanda: item.id})}>
                                                    <Text style={styles.btnText}>Elimina</Text>
                                                </TouchableOpacity>
                                            </View>
                                            ) : null }
                                            <FlatList
                                                data={item.choices}
                                                extraData={this.state}
                                                style={{marginTop: 10, flex: 1}}
                                                renderItem={({ item: innerData }) =>
                                                    <View style={styles.data}>
                                                        { global.logged_in == true && !this.state.creator &&
                                                        this.state.submission_exist  && this.state.dataSource2.answers[this.getIndex(item.id)].choice == innerData.id  &&
                                                        (innerData.text == "Other" || innerData.text == "Altro")? (
                                                            <Text style={styles.textSub}>- {innerData.text}: "{sub.answers[this.getIndex(item.id)].extra_text}"</Text>
                                                        ) : null }
                                                        { global.logged_in == true && !this.state.creator && this.state.submission_exist  && this.state.dataSource2.answers[this.getIndex(item.id)].choice == innerData.id && innerData.text != "Other" && innerData.text != "Altro"? (
                                                            <Text style={styles.textSub}>- {innerData.text}</Text>
                                                        ): null }
                                                        { global.logged_in == true && this.state.creator && (innerData.count).length>0 ?(
                                                        <Text style={styles.textData}>- {innerData.text} ({innerData.count[0].count/data.sub_completed.length*100} %) </Text>
                                                        ): null }
                                                        { global.logged_in == true && this.state.creator && (this.state.dataSource.status == 1 || this.state.dataSource.status == 2) && (innerData.count).length == 0  ?(
                                                        <Text style={styles.textData}>- {innerData.text} (0 %) </Text>
                                                        ): null }
                                                        { global.logged_in == true && this.state.creator && this.state.dataSource.status == 0 && item.type != 1?(
                                                            <View style={styles.entry2}>
                                                                <Text>- {innerData.text}</Text>
                                                                <TouchableOpacity onPress={() => this.props.navigation.navigate('ModificaOpzione', {id_sondaggio: this.state.id_sondaggio, id_domanda: item.id, id_opzione: innerData.id})}>
                                                                    <Text style={styles.btnText}>Modifica</Text>
                                                                </TouchableOpacity>
                                                                <TouchableOpacity onPress={() => this.props.navigation.navigate('EliminaOpzione', {id_sondaggio: this.state.id_sondaggio, id_domanda: item.id, id_opzione: innerData.id})}>
                                                                    <Text style={styles.btnText}>Elimina</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                        ): null}
                                                        { global.logged_in == true && this.state.creator && this.state.dataSource.status == 0 && item.type == 1 ?(
                                                            <View style={styles.entry2}>
                                                                <Text>- {innerData.text}</Text>
                                                            </View>
                                                        ): null}
                                                      { global.logged_in == false || (!this.state.creator && !this.state.submission_exist) ||
                                                      (!this.state.creator && this.state.submission_exist && this.state.dataSource2.answers[this.getIndex(item.id)].choice != innerData.id) ? (
                                                          <Text style={styles.textData}>- {innerData.text}</Text>
                                                      ) : null }
                                                  </View>
                                                  }/>
                                        </View>
                                    } />
                                    <View style={{alignItems: 'center'}}>
                                        <View style={styles.controlli}>
                                        { global.logged_in == true && this.state.creator  && data.status == 0 ? (
                                        <View style={styles.buttonview}>
                                            <Button title="Aggiungi Domanda" onPress={() => this.props.navigation.navigate('CreaDomanda', {id_sondaggio: this.state.id_sondaggio})}/>
                                        </View>
                                        ) : null }
                                        </View>
                                        { global.logged_in == true && this.state.creator  && data.status == 0 ? (
                                        <View style={{marginTop: 20, marginBottom: 5}}>
                                            <Text>I campi "altro" saranno aggiunti all'attivazione</Text>
                                        </View>
                                        ) : null }
                                    </View>
                            </View>
                        </Card>
                    </View>
                </ScrollView>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    screen: {
        flex: 1
    },
    title: {
        fontSize: 20,
        marginVertical: 10,
        textAlign: 'center'
    },
    data: {
        paddingTop: 20,
        paddingLeft: 10
    },
    entry: {
        paddingTop: 10,
        flexDirection: 'row',
    },
    entry2: {
        paddingTop: 10,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    textTitle: {
        fontWeight: 'bold'
    },
    textData: {
        width: width - width / 3
    },
    btnText: {
        fontWeight: 'bold',
        color: 'red',
        paddingLeft: 10,


    },
    textSub: {
        fontWeight: 'bold',
        width: width - width / 3,
        color: 'red'
    },
    controlli: {
        flexDirection: 'row',
        paddingTop: 20,
        paddingRight: 28
    },
    buttonview: {
        width: 120,
        paddingRight: 5,
        paddingLeft: 5
    },
    inputContainer: {
        minWidth: '96%',
        flexDirection: 'row'
    },
    contentbar: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
      },
    leftcontainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    rightcontainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    flatlistview: {
        flex: 1,
        width: '100%',
        justifyContent: 'space-between',
        alignItems: 'center'
    }
});

export default SurveyDetail;