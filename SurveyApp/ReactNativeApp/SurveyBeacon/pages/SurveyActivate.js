import React, { Component } from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';
import CustomHeader from '../components/Header';
import Card from '../components/Card';
import { Dimensions } from 'react-native';

const {width, height} = Dimensions.get('window');

let id_sondaggio;
let title;
let state;

class SurveyActivate extends Component {

    constructor(props){
        super(props);
        this.state = {
            error_msg: "",
            data: this.props.navigation.state.params.data_sondaggio,
            id_sondaggio: this.props.navigation.state.params.id_sondaggio,
        }
    }

    check_opzioni () {
        for (let i = 0; i< this.state.data.questions.length; ++i) {
            if (this.state.data.questions[i].choices.length == 0)
                return false;
        }
        return true;
    }

    attivaSondaggio = () => {
        let tags = [];
        for (let i=0; i<data.tags.length; ++i) {
            tags.push(data.tags[i].id);
        }
        if (this.state.data.questions.length > 0 && this.check_opzioni()) {
            fetch('http:/192.168.1.34:8000/api/survey/' + this.props.navigation.state.params.id_sondaggio +'/edit', {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Token ' + global.user_key,
                },
                body: JSON.stringify({
                  title: this.state.data.title,
                  description: this.state.data.description,
                  thanks_message: this.state.data.thanks_message,
                  category: this.state.data.category.id,
                  tags: tags,
                  status: 1,
              }),
            })
                .then(res => res.json())
                .then((res) => {
                    if (res.id != null) {
                        this.props.navigation.goBack(null);
                    }
                })
                .catch((error) => {
                    console.log("Errore");
                })
        }
        else {
            this.setState({error_message: "Errore: nel sondaggio sono presenti domanda senza opzioni o nessuna domanda"});
        }
    };

    render() {
            title = "Confermi di voler ATTIVARE il sondaggio?";

            return (

            <View style={styles.screen}>
                <CustomHeader parent={this.props} />

                <View style={{alignItems: 'center'}}>
                    <Card style={styles.inputContainer}>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={styles.title}>{title}</Text>
                        </View>

                        <View style={styles.controlli}>
                            <View style={styles.buttonview}>
                                <Button title="Indietro" onPress={() => this.props.navigation.goBack(null)}/>
                            </View>
                            <View style={styles.buttonview}>
                                <Button title="Conferma" onPress={() => {this.attivaSondaggio()}} />
                            </View>
                        </View>
                    </Card>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1
    },
    title: {
        fontSize: 20,
        marginVertical: 10
    },
    buttonview: {
        width: 110,
        paddingRight: 5,
        paddingLeft: 5
    },
    inputContainer: {
        minWidth: '96%'
    },
    controlli: {
        flexDirection: 'row',
        paddingTop: 20
    }
});

export default SurveyActivate;