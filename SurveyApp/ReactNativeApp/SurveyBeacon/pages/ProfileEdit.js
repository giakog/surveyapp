import React, { Component } from 'react';
import { View, Text, StyleSheet, Button, Image, TextInput, StatusBar } from 'react-native';
import CustomHeader from '../components/Header';
import Card from '../components/Card';
import { TouchableOpacity, TouchableWithoutFeedback, ScrollView } from 'react-native-gesture-handler';
import { Dimensions, ActivityIndicator } from 'react-native';
import { IconButton } from 'react-native-paper';
import { Picker } from 'native-base';

import CountryPicker from 'react-native-country-picker-modal';


const {width, height} = Dimensions.get('window');

class ProfileEdit extends Component {
       constructor(props){
           StatusBar.setHidden(true);
           super(props);
           this.state ={
               cca2: "IT",
               error_message: "",
               cs: "",
               ws: "",
               ed: "",
               genere: "",
               citta: "",
           }
       }

    componentDidMount() {
        this.fetchProfilo();
        this.willFocusSubscription = this.props.navigation.addListener(
          'willFocus',
          () => {
            this.setState({
                isLoading: true,
            }, function(){

            });
            this.fetchProfilo();
          }
        );
    }

    componentWillUnmount() {
    this.willFocusSubscription.remove();
    }

    fetchProfilo() {
        return fetch('http:/192.168.1.34:8000/api/profile/my', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Token ' + global.user_key,
            }
        })
        .then((response) => response.json())
        .then((responseJson) => {

        this.setState({
            isLoading: false,
            genere: responseJson.sex,
            cs: responseJson.civil_state,
            ed: responseJson.education,
            ws: responseJson.working_state,
            cca2: responseJson.country,
            citta: responseJson.city,
        }, function(){

        });
        })
        .catch((error) =>{
            console.log("Error",error);
            this.setState({error_message:"Nessun profilo trovato", isLoading:false})
        });
    }

    modificaProfilo = () => {
        if (this.state.ws != "" && this.state.cs != "" && this.state.ed != "" && this.state.citta != "" && this.state.paese != ""
            && this.state.genere != "") {
            console.log(JSON.stringify({
                    city: this.state.citta,
                    country: this.state.cca2,
                    education: this.state.ed,
                    civil_state: this.state.cs,
                    working_state: this.state.ws,
                    sex: this.state.genere,
                }
            ));
            if(global.gender == "" ||global.ed == "" || global.cs == "" || global.ws == "" || global.paese =="" || global.citta == "") {
                fetch('http:/192.168.1.34:8000/api/profile/create',
                    {
                        method: 'POST',
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json',
                            'Authorization': 'Token ' + global.user_key,
                        },
                        body: JSON.stringify({
                            city: this.state.citta,
                            country: this.state.cca2,
                            education: this.state.ed,
                            civil_state: this.state.cs,
                            working_state: this.state.ws,
                            sex: this.state.genere,
                        }),
                    })
                    .then(res => res.json())
                    .then((res) => {
                        if (res.user.id != null) {
                            global.user_id = res.user.id;
                            global.gender = this.state.genere;
                            global.cs = this.state.cs;
                            global.ed = this.state.ed;
                            global.ws = this.state.ws;
                            global.country = this.state.cca2;
                            global.city = this.state.citta;
                            this.props.navigation.navigate("Categorie");
                        } else {
                            this.setState({error_message: "Errore: " + JSON.stringify(res)});
                        }
                    })
                    .catch((error) => {
                        this.setState({error_message: "Errore: " + error});
                        console.log("Errore")
                    })
            }
            else {
                fetch('http:/192.168.1.34:8000/api/profile/my',
                    {
                        method: 'PUT',
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json',
                            'Authorization': 'Token ' + global.user_key,
                        },
                        body: JSON.stringify({
                            city: this.state.citta,
                            country: this.state.cca2,
                            education: this.state.ed,
                            civil_state: this.state.cs,
                            working_state: this.state.ws,
                            sex: this.state.genere,
                        }),
                    })
                    .then(res => res.json())
                    .then((res) => {
                        if (res.user.id != null) {
                            global.gender = this.state.genere;
                            global.cs = this.state.cs;
                            global.ed = this.state.ed;
                            global.ws = this.state.ws;
                            global.country = this.state.cca2;
                            global.city = this.state.citta;
                            this.setState({error_message: ""});
                            this.props.navigation.goBack(null);
                        } else {
                            this.setState({error_message: "Errore: " + JSON.stringify(res)});
                        }
                    })
                    .catch((error) => {
                        this.setState({error_message: "Errore: " + error});
                        console.log("Errore")
                    })
            }
        }
        else {
            this.setState({error_message: "ERRORE: Campi obbligatori vuoti."});
        }
    };

    render() {

        if(this.state.isLoading){
            return(
                <View style={{flex: 1, paddingTop: height / 2}}>
                    <ActivityIndicator/>
                </View>
            )
        }

        return (

            <View style={styles.screen}>
                <CustomHeader parent={this.props} />

                <View style={styles.contentbar}>
                    <View style={styles.leftcontainer}>
                        <IconButton icon="arrow-left" onPress={() => this.props.navigation.goBack(null)} />
                    </View>
                    <Text style={styles.title}>
                        Modifica profilo
                    </Text>
                    <View style={styles.rightcontainer}></View>
                </View>

                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{alignItems: 'center'}}>
                        <Card style={styles.inputContainer}>
                            <View style={styles.data}>
                                <View style={{flexDirection: 'row'}}>
                                    <View>
                                        <View style={styles.entryTitle}>
                                            <Text style={styles.textTitle}>Genere:</Text>
                                            <Text style={styles.asteriskStyle}>*</Text>
                                        </View>
                                        <View style={styles.entryTitle}>
                                            <Text style={styles.textTitle}>Stato civile: </Text>
                                            <Text style={styles.asteriskStyle}>*</Text>
                                        </View>
                                        <View style={styles.entryTitle}>
                                            <Text style={styles.textTitle}>Educazione </Text>
                                            <Text style={styles.asteriskStyle}>*</Text>
                                        </View>
                                        <View style={styles.entryTitle}>
                                            <Text style={styles.textTitle}>Stato lavorativo: </Text>
                                            <Text style={styles.asteriskStyle}>*</Text>
                                        </View>
                                        <View style={styles.entryTitle}>
                                            <Text style={styles.textTitle}>Paese: </Text>
                                            <Text style={styles.asteriskStyle}>*</Text>
                                        </View>
                                        <View style={styles.entryTitle}>
                                            <Text style={styles.textTitle}>Città: </Text>
                                            <Text style={styles.asteriskStyle}>*</Text>
                                        </View>
                                    </View>

                                    <View>
                                        <Picker
                                            style={styles.picker} itemStyle={styles.pickerItem}
                                            selectedValue={this.state.genere}
                                            onValueChange={(itemValue) => this.setState({genere: itemValue})}
                                            >
                                            <Picker.Item label="-" value="" />
                                            <Picker.Item label="Maschio" value="maschio" />
                                            <Picker.Item label="Femmina" value="femmina" />
                                            <Picker.Item label="Altro" value="altro" />
                                        </Picker>
                                        <Picker
                                            style={styles.picker} itemStyle={styles.pickerItem}
                                            selectedValue={this.state.cs}
                                            onValueChange={(itemValue) => this.setState({cs: itemValue})}
                                            >
                                            <Picker.Item label="-" value="" />
                                            <Picker.Item label="Single" value="single" />
                                            <Picker.Item label="Sposato/a" value="sposato/a" />
                                            <Picker.Item label="Divorziato/a" value="divorziato/a" />
                                            <Picker.Item label="Vedovo/a" value="vedovo/a" />
                                        </Picker>
                                        <Picker
                                            style={styles.picker} itemStyle={styles.pickerItem}
                                            selectedValue={this.state.ed}
                                            onValueChange={(itemValue) => this.setState({ed: itemValue})}
                                            >
                                            <Picker.Item label="-" value="" />
                                            <Picker.Item label="Scuola primaria" value="elementare" />
                                            <Picker.Item label="Scuola secondaria 1° grado" value="media" />
                                            <Picker.Item label="Scuola secondaria 2° grado" value="diploma" />
                                            <Picker.Item label="Università" value="laurea" />
                                        </Picker>
                                        <Picker
                                            style={styles.picker} itemStyle={styles.pickerItem}
                                            selectedValue={this.state.ws}
                                            onValueChange={(itemValue) => this.setState({ws: itemValue})}
                                            >
                                            <Picker.Item label="-" value="" />
                                            <Picker.Item label="Studente" value="studente" />
                                            <Picker.Item label="Disoccupato" value="disoccupato" />
                                            <Picker.Item label="Occupato" value="occupato" />
                                            <Picker.Item label="Ritirato" value="ritirato" />
                                        </Picker>
                                        <CountryPicker
                                            withFilter={true}
                                            withCountryNameButton={true}
                                            countryCode={this.state.cca2}
                                            onSelect={(value)=> this.setState({country: value, cca2: value.cca2})}
                                            cca2={this.state.cca2}
                                            translation='ita'
                                          />
                                        <View style={styles.textContainer}>
                                            <TextInput
                                                style={{fontSize:16}}
                                                editable maxLength={245}
                                                       value={this.state.citta}
                                            ref={input => { this.txtText = input }}
                                            onChangeText={(value) => this.setState({citta: value})}/>
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.controlli}>
                                    <View style={styles.buttonview}>
                                        <Button title="Applica modifiche" onPress={() => {
                                            this.modificaProfilo();}} />
                                    </View>
                                </View>

                                <View style={{paddingTop: 10}}></View>
                                <Text style={{color: 'red'}}>{this.state.error_message}</Text>
                                <View style={{paddingTop: 10}}></View>

                                <View style={{flexDirection: 'row', marginTop: 20, marginBottom: 5}}>
                                    <Text>I campi contrassegnati con</Text>
                                    <Text style={styles.asteriskStyle}>*</Text>
                                    <Text>sono obbligatori.</Text>

                                </View>
                            </View>
                        </Card>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1
    },
    title: {
        fontSize: 20,
        marginVertical: 10
    },
    buttonview: {
        width: 110,
        paddingRight: 5,
        paddingLeft: 5
    },
    inputContainer: {
        minWidth: '96%'
    },
    controlli: {
        paddingTop: 20,
        paddingRight: 5,
        alignItems: 'center'
    },
    data: {
        paddingTop: 20,
        paddingLeft: 10
    },
    entryTitle: {
        marginBottom: 5,
        marginTop: 9,
        flexDirection: 'row'
    },
    textTitle: {
        fontWeight: 'bold'
    },
    contentbar: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
      },
    leftcontainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    rightcontainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    textContainer: {
        borderWidth: 1,
        height: 28,
        width: width - width / 2,
        marginLeft: 10,
        marginBottom: 3,
        marginTop: 3
    },
    asteriskStyle: {
        marginLeft: 3,
        marginRight: 3,
        color: 'red'
    },
    picker: {
        marginLeft: 10,
        width: width - width / 2,
        height: 28,
        backgroundColor: '#e7e7e7',
        marginBottom: 3,
        marginTop: 3
    },
    pickerItem: {
        color: 'white'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    titleText: {
        color: '#000',
        fontSize: 25,
        marginBottom: 25,
        fontWeight: 'bold',
    },
    pickerTitleStyle: {
        justifyContent: 'center',
        flexDirection: 'row',
        alignSelf: 'center',
        fontWeight: 'bold',
        flex: 1,
        marginLeft: 10,
        fontSize: 16,
        color: '#000',
    },
    pickerStyle: {
        height: 60,
        width: 250,
        marginBottom: 10,
        justifyContent: 'center',
        padding: 10,
        borderWidth: 2,
        borderColor: '#303030',
        backgroundColor: 'white',
    },
    selectedCountryTextStyle: {
        paddingLeft: 5,
        paddingRight: 5,
        color: '#000',
        textAlign: 'right',
    },
    countryNameTextStyle: {
        paddingLeft: 10,
        color: '#000',
        textAlign: 'right',
    },
    searchBarStyle: {
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'row',
        marginLeft: 8,
        marginRight: 10,
    },
});

export default ProfileEdit;