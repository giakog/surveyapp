import React, { Component } from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';
import CustomHeader from '../components/Header';
import Card from '../components/Card';
import { Dimensions } from 'react-native';

const {width, height} = Dimensions.get('window');

let id_sondaggio;
let title;
let state;

class SurveyDeleteBlock extends Component {

    constructor(props){
        super(props);
        this.state = {
            data: this.props.navigation.state.params.data_sondaggio,
            id_sondaggio: this.props.navigation.state.params.id_sondaggio,
        }
    }

    eliminainterrompiSondaggio = () => {
        if (this.props.navigation.state.params.id_op == 0) {
            fetch('http:/192.168.1.34:8000/api/survey/detail/' + this.props.navigation.state.params.id_sondaggio +'/creator', {
                method: 'DELETE',
                headers: {
                    'Authorization': 'Token ' + global.user_key
                }
            })
                .then(res => {
                    console.log(this.state.data.category);
                    this.props.navigation.pop(2);
                })
                .catch((error) => {
                    console.log(this.props.navigation.state.params.data_sondaggio.category);
                });
        }
        else {
            fetch('http:/192.168.1.34:8000/api/survey/' + this.props.navigation.state.params.id_sondaggio + '/edit', {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Token ' + global.user_key
                },
                body: JSON.stringify({
                    id: id_sondaggio,
                    creator: global.user_id,
                    title: this.state.data.title,
                    category: this.state.data.category,
                    description: this.state.data.description,
                    tags: this.state.data.tags,
                    status: "2",
                }),
            })
                .then(res => res.json())
                .then((res) => {
                    if (res.id != null) {
                        this.props.navigation.goBack(null);
                    }
                })
                .catch((error) => {
                   console.log(error);
                });
        }
    }

    render() {
        id_sondaggio = this.props.navigation.state.params.id_sondaggio;
        state = this.props.navigation.state.params.id_op;
        if (state == 0)
            title = "Confermi di voler eliminare il sondaggio?";
        else
            title = "Confermi di voler interrompere il sondaggio?";
        return (

            <View style={styles.screen}>
                <CustomHeader parent={this.props} />

                <View style={{alignItems: 'center'}}>
                    <Card style={styles.inputContainer}>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={styles.title}>{title}</Text>
                        </View>

                        <View style={styles.controlli}>
                            <View style={styles.buttonview}>
                                <Button title="Indietro" onPress={() => this.props.navigation.goBack(null)}/>
                            </View>
                            <View style={styles.buttonview}>
                                <Button title="Conferma" onPress={() => {this.eliminainterrompiSondaggio();}} />
                            </View>
                        </View>
                    </Card>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1
    },
    title: {
        fontSize: 20,
        marginVertical: 10
    },
    buttonview: {
        width: 110,
        paddingRight: 5,
        paddingLeft: 5
    },
    inputContainer: {
        minWidth: '96%'
    },
    controlli: {
        flexDirection: 'row',
        paddingTop: 20
    }
});

export default SurveyDeleteBlock;