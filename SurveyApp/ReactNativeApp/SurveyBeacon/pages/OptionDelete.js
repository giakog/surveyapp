import React, { Component } from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';
import CustomHeader from '../components/Header';
import Card from '../components/Card';
import { Dimensions } from 'react-native';

const {width, height} = Dimensions.get('window');

let title;

class OptionDelete extends Component {

    constructor(props){
        super(props);
        this.state = {
            id_sondaggio: this.props.navigation.state.params.id_sondaggio,
            id_domanda: this.props.navigation.state.params.id_domanda,
            id_opzione: this.props.navigation.state.params.id_opzione,
        }
    }

    eliminaOpzione = () => {
        console.log(this.state)
        fetch('http:/192.168.1.34:8000/api/survey/' + this.props.navigation.state.params.id_sondaggio +'/question/' +
            this.props.navigation.state.params.id_domanda + '/choice/' + this.state.id_opzione + '/edit', {
            method: 'DELETE',
            headers: {
                'Authorization': 'Token ' + global.user_key
            }
        })
            .then(res => {
                this.props.navigation.goBack(null);
            })
            .catch((error) => {
                console.log("Errore")
                this.eliminaDomanda();
            });
    };

    render() {
        title = "Confermi di voler eliminare l'opzione?";

        return (

            <View style={styles.screen}>
                <CustomHeader parent={this.props} />

                <View style={{alignItems: 'center'}}>
                    <Card style={styles.inputContainer}>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={styles.title}>{title}</Text>
                        </View>

                        <View style={styles.controlli}>
                            <View style={styles.buttonview}>
                                <Button title="Indietro" onPress={() => this.props.navigation.goBack(null)}/>
                            </View>
                            <View style={styles.buttonview}>
                                <Button title="Conferma" onPress={() => {this.eliminaOpzione();}} />
                            </View>
                        </View>
                    </Card>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1
    },
    title: {
        fontSize: 20,
        marginVertical: 10
    },
    buttonview: {
        width: 110,
        paddingRight: 5,
        paddingLeft: 5
    },
    inputContainer: {
        minWidth: '96%'
    },
    controlli: {
        flexDirection: 'row',
        paddingTop: 20
    }
});

export default OptionDelete;