import React, { Component } from 'react';
import { View, Text, StyleSheet, Button, Image, Dimensions, ActivityIndicator, FlatList, YellowBox } from 'react-native';
import CustomHeader from '../components/Header';
import Card from '../components/Card';
import { TouchableOpacity, TouchableWithoutFeedback, ScrollView } from 'react-native-gesture-handler';

const {width, height} = Dimensions.get('window');

class CategoryList extends Component {
    constructor(props){
        super(props);
        this.state ={
            isLoading: true,
        }
    }

    componentDidMount() {
        this.fetchTags();
        this.fetchCategories();
        this.willFocusSubscription = this.props.navigation.addListener(
            'willFocus',
            () => {
                this.setState({
                    isLoading: true,
                }, function(){

                });
                this.fetchTags();
                this.fetchCategories();
            }
        );
    }

    componentWillUnmount() {
        this.willFocusSubscription.remove();
    }
        fetchTags() {
        return fetch('http://192.168.1.34:8000/api/tag/list',
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            })
        .then(response => response.json())
        .then((responseJson) => {
            if(global.tag_list.length == 0)
                global.tag_list = responseJson;
            this.setState({
                isLoading: false,
                dataSource: responseJson,
            }, function(){

            });
        })
        .catch((error) => {
            this.fetchTags();
        })
    }

    fetchCategories() {
        return fetch('http://192.168.1.34:8000/api/category/list',
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            })
        .then(response => response.json())
        .then((responseJson) => {
            if (global.category_list.length == 0)
                global.category_list = responseJson;
            this.setState({
                isLoading: false,
                dataSource: responseJson,
            }, function(){

            });
        })
        .catch((error) => {
            this.fetchCategories();
        })
    }
     render() {

        if(this.state.isLoading){
            return(
                <View style={{flex: 1, paddingTop: height / 2}}>
                    <ActivityIndicator/>
                </View>
            )
        }

        return (

            <View style={styles.screen}>

                <View style={{alignSelf: 'flex-start', width: '100%', alignItems: 'center'}}>
                    <CustomHeader parent={this.props} />
                    <View style={styles.contentbar}>
                        <Text style={styles.title}>
                            Categorie
                        </Text>
                    </View>
                </View>
                <View style={styles.flatlistview}>
                    <FlatList
                        style={{flex: 1}}
                        data={global.category_list}
                        renderItem={({item, index}) =>
                        <TouchableOpacity style={styles.touchableopacity} activeOpacity={.8} onPress={() => {global.category = item.title;
                            this.props.navigation.navigate('ListaSondaggi', {category_id: item.id});}}>
                            <Card style={styles.inputContainer}>
                                <View style={styles.data}>
                                    { item.title != 'All' ? (
                                        <Text style={styles.annuncioTitle} numberOfLines={1}>{item.title}</Text>
                                    ): null}
                                    { item.title == 'All' ? (
                                        <Text style={styles.annuncioTitle} numberOfLines={1}>Tutte</Text>
                                    ): null}
                                </View>
                            </Card>
                        </TouchableOpacity>
                        }
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    screen: {
        flex: 1,
        alignItems: 'center'
    },
    annuncioLogo: {
        width: 80,
        height: 80
    },
    title: {
        fontWeight:"bold",
        fontSize: 30,
        color:"#4169e1",
        marginTop: 20
    },
    inputContainer: {
        flex: 1,
        minWidth: '96%',
        flexDirection: 'row'
    },
    annuncioTitle: {
        fontSize: 18,
        fontWeight: 'bold',
        width: width - width / 2.6
    },
    annuncioSubtitle: {
        width: width - width / 2.6
    },
    data: {
        flex: 1
    },
    image: {
        paddingRight: 10
    },
    textInline: {
        flexDirection: 'row'
    },
    touchableopacity: {
        flex: 1,
        alignItems: 'center'
    },
    picker: {
        width: '100%',
        height: 40,
        backgroundColor: '#e7e7e7',
        borderColor: 'black',
        borderWidth: 3
    },
    pickerItem: {
        color: 'white'
    },
    contentbar: {
        height: 70,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    centercontainer: {
        flex: 1,
        flexDirection: 'row',
    },
    rightcontainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    flatlistview: {
        flex: 1,
        width: '100%',
        justifyContent: 'space-between',
        alignItems: 'center'
    }
});
export default CategoryList;
