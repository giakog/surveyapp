import React from "react";
import {ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View} from "react-native";
import CustomHeader from '../components/Header';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      error: ""
    }
  }

  fetchUserId() {
      fetch('http://192.168.1.34:8000/api/profile/search/' + this.state.username.toLowerCase())
          .then((user_response) => user_response.json())
          .then((user_responseJson) => {
              console.log(user_responseJson);
              global.user_id = user_responseJson.user.id;
              global.gender = user_responseJson.sex;
              global.cs = user_responseJson.civil_state;
              global.ed = user_responseJson.education;
              global.ws = user_responseJson.working_state;
              global.country = user_responseJson.country;
              global.city = user_responseJson.city;
              if(global.user_id != -1)
                  global.profile_complete = true
          })
          .catch((error) =>{
              this.props.navigation.navigate('ModificaProfilo')
          });
  }
  loginExecute = () => {
      fetch('http://192.168.1.34:8000/api/rest-auth/login/',
          {
              method: 'POST',
              headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
              },
              body: JSON.stringify({
                  username: this.state.username.toLowerCase(),
                  password: this.state.password,
              }),
          })
          .then(res => res.json())
          .then((res) => {
              if(res.key != null) {
                  global.user_key = res.key;
                  global.logged_in = true;
                  global.username = this.state.username.toLowerCase();
                  this.fetchUserId();
                  this.clearFields();
                  this.props.navigation.goBack(null);
              } else {
                  this.setState({error_message: "Errore: username o password errati."});
              }
          })
          .then(obj =>  {
              callback(obj)
          })
          .catch((error) => {
              this.setState({error_message: "Errore: Login non riuscito."});
          })
  };

  clearFields = () => {
      this.setState({username: ""});
      this.setState({password: ""});
      this.setState({error_message: ""});
      this.txtUsername.clear();
      this.txtPassword.clear();
  };
  render(){
      return (
          <ScrollView contentContainerStyle={styles.container}>
              <CustomHeader parent={this.props} />
              <Text style={styles.logo}>Login</Text>
              <View style={styles.inputView} >
                  <TextInput
                      style={styles.inputText}
                      placeholder="Username"
                      placeholderTextColor="black"
                      ref={input => { this.txtUsername = input }}
                      onChangeText={text => this.setState({username:text})}/>
              </View>
              <View style={styles.inputView} >
                  <TextInput secureTextEntry
                style={styles.inputText}
                placeholder="Password"
                placeholderTextColor="black"
                             ref={input => { this.txtPassword = input }}
                onChangeText={text => this.setState({password:text})}/>
            </View>
               <Text style={{color: 'red'}}>{this.state.error}</Text>
            <TouchableOpacity style={styles.loginBtn} onPress={this.loginExecute}>
              <Text style={styles.loginText}>Accedi</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('Registration')}>
              <Text style={styles.loginText}>Registrati</Text>
            </TouchableOpacity>
          </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  logo:{
      fontWeight:"bold",
      fontSize:40,
      marginTop: 20,
      color:"#4169e1",
      marginBottom:40
  },
  inputView:{
    width:"80%",
    backgroundColor:"#f5f5f5",
    borderRadius:25,
    height:50,
    marginBottom:20,
    justifyContent:"center",
    padding:20
  },
  inputText:{
    height:50,
    color:"black"
  },
  loginBtn:{
    width:"80%",
    backgroundColor:"#4169e1",
    borderRadius:25,
    height:50,
    alignItems:"center",
    justifyContent:"center",
    marginTop:40,
    marginBottom:10
  },
  loginText:{
    color:"black"
  }
});
export default Login;



