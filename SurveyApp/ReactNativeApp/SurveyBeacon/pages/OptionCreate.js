import React, { Component } from 'react';
import {View, Text, StyleSheet, Button, TextInput, ActivityIndicator, FlatList} from 'react-native';
import CustomHeader from '../components/Header';
import Card from '../components/Card';
import { TouchableOpacity, TouchableWithoutFeedback, ScrollView } from 'react-native-gesture-handler';
import { Dimensions } from 'react-native';
import { IconButton } from 'react-native-paper';

const {width, height} = Dimensions.get('window');

class OptionCreate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            id_sondaggio: this.props.navigation.state.params.id_sondaggio,
            id_domanda: this.props.navigation.state.params.id_domanda,
            error_msg: "",
            text: "",
        }
    }

    componentDidMount() {
        this.fetchOpzioniDomanda();

        this.willFocusSubscription = this.props.navigation.addListener(
          'willFocus',
          () => {
            this.setState({
                isLoading: true,
            }, function(){

            });
            this.fetchOpzioniDomanda();
          }
        );
    }

    componentWillUnmount() {
        this.willFocusSubscription.remove();
    }

    fetchOpzioniDomanda() {
        return fetch('http:/192.168.1.34:8000/api/survey/' +
            this.props.navigation.state.params.id_sondaggio + '/question/'+ this.state.id_domanda + '/choice-list')
            .then((response) => response.json())
            .then((responsejson) => {
                this.setState( {
                    isLoading: false,
                    dataSource: responsejson
                }, function () {

                });
            })
    }

    creaOpzione = () => {
        console.log(this.state.text, this.state.type, this.state.other_field)
        if (this.state.text != "" && this.state.type != -1) {
            fetch('http:/192.168.1.34:8000/api/survey/'+ this.state.id_sondaggio + '/question/' + this.state.id_domanda +
                '/choice/create',
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Token ' + global.user_key,
                },
              body: JSON.stringify({
                  survey: this.state.id_sondaggio,
                  question: this.state.id_domanda,
                  text: this.state.text,
              }),
            })
                .then(res => res.json())
                .then((res) => {
                    if (res.id != null) {
                        this.clearFields();
                        this.state.isLoading = true;
                        this.fetchOpzioniDomanda();
                    } else {
                        this.setState({error_message: "Errore: controlla i campi inseriti e riprova."});
                    }
                })
                .catch((error) => {
                   console.log("Errore")
                })
        } else {
            this.setState({error_message: "Errore: assicurati di riempire tutti i campi."});
        }
    };

    clearFields = () => {
        this.setState({
            error_message: "",
            text: "",
        });
        this.txtText.clear();
    };

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: height / 2}}>
                    <ActivityIndicator/>
                </View>
            )
        }
        console.log(this.state.dataSource)
        return (
            <View style={styles.screen}>
                <CustomHeader parent={this.props} />

                <View style={styles.contentbar}>
                    <View style={styles.leftcontainer}>
                        <IconButton icon="arrow-left" onPress={() => this.props.navigation.goBack(null)} />
                    </View>
                    <Text style={styles.title}>
                        Crea Opzioni
                    </Text>
                    <View style={styles.rightcontainer}></View>
                </View>

                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{alignItems: 'center'}}>
                        <Card style={styles.inputContainer}>
                            <View style={styles.data}>
                                <FlatList
                                    data={this.state.dataSource}
                                    style={{marginTop: 10, flex: 1}}
                                    renderItem={({ item, index }) =>
                                    <View style={styles.data}>
                                        <Text style={styles.textSub}>- {item.text}</Text>
                                    </View>
                                    }/>
                                <View style={{flexDirection: 'row'}}>
                                    <View>
                                        <View style={styles.entryTitle}>
                                            <Text style={styles.textTitle}>Testo:</Text>
                                            <Text style={styles.asteriskStyle}>*</Text>
                                        </View>
                                    </View>
                                    <View>
                                        <View style={styles.textContainer}>
                                            <TextInput editable maxLength={245}
                                            ref={input => { this.txtText = input }}
                                            onChangeText={(value) => this.setState({text: value})}/>
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.controlli}>
                                    <View style={styles.buttonview}>
                                        <Button title="Aggiungi Opzione" onPress={() => {
                                            this.creaOpzione();}} />
                                    </View>
                                </View>

                                <View style={{paddingTop: 10}}></View>
                                <Text style={{color: 'red'}}>{this.state.error_message}</Text>
                                <View style={{paddingTop: 10}}></View>

                                <View style={{flexDirection: 'row', marginTop: 20, marginBottom: 5}}>
                                    <Text>I campi contrassegnati con</Text>
                                    <Text style={styles.asteriskStyle}>*</Text>
                                    <Text>sono obbligatori.</Text>

                                </View>
                            </View>
                        </Card>
                    </View>
                </ScrollView>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1
    },
    title: {
        fontSize: 20,
        marginVertical: 10
    },
    buttonview: {
        width: 150,
        paddingRight: 5,
        paddingLeft: 5
    },
    inputContainer: {
        minWidth: '96%'
    },
    controlli: {
        paddingTop: 20,
        paddingRight: 5,
        alignItems: 'center'
    },
    data: {
        paddingTop: 20,
        paddingLeft: 10
    },
    entryTitle: {
        marginBottom: 5,
        marginTop: 9,
        flexDirection: 'row'
    },
    textTitle: {
        fontWeight: 'bold'
    },
    contentbar: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
      },
    leftcontainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    rightcontainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    textContainer: {
        borderWidth: 1,
        height: 28,
        width: width - width / 2,
        marginLeft: 10,
        marginBottom: 3,
        marginTop: 10
    },
    caricaStyle: {
        marginBottom: 3,
        marginTop: 3,
        height: 28,
        width: 100,
        marginLeft: 10,
        borderWidth: 1,
        alignItems: 'center'
    },
    checkBoxStyle: {
        marginLeft: 10,
        marginBottom: 6,
        marginTop: 3
    },
    asteriskStyle: {
        marginLeft: 3,
        marginRight: 3,
        color: 'red'
    },
    picker: {
        marginLeft: 10,
        width: width - width / 2,
        height: 28,
        backgroundColor: '#e7e7e7',
        marginBottom: 3,
        marginTop: 3
    },
    pickerItem: {
        color: 'white'
    }
});
export default OptionCreate;