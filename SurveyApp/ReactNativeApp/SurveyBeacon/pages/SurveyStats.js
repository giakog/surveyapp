import React, { Component } from 'react';
import {View, Text, StyleSheet, Button, Image, ActivityIndicator, FlatList, LogBox, TextInput} from 'react-native';
import {Picker} from "native-base";
import CustomHeader from '../components/Header';
import Card from '../components/Card';
import { TouchableOpacity, TouchableWithoutFeedback, ScrollView } from 'react-native-gesture-handler';

import { Dimensions } from 'react-native';
import { IconButton } from 'react-native-paper';
import CountryPicker from "react-native-country-picker-modal";
import { Table, TableWrapper, Row, Rows, Col } from 'react-native-table-component';

const {width, height} = Dimensions.get('window');


class SurveyStats extends Component {

    constructor(props){
        super(props);
        this.state ={
            isLoading: true,
            gender: this.props.navigation.state.params.gender,
            cs: this.props.navigation.state.params.cs,
            ed: this.props.navigation.state.params.ed,
            ws: this.props.navigation.state.params.ws,
            city: this.props.navigation.state.params.city,
            country: this.props.navigation.state.params.country,
            pick_country: this.props.navigation.state.params.pick_country,
            question: this.props.navigation.state.params.question,
            choice: this.props.navigation.state.params.choice,
            id_sondaggio: this.props.navigation.state.params.id_sondaggio,
            flexs: [[0], [1], [1,1],[1,1,1],[1,1,1,1],[1,1,1,1,1],[1,1,1,1,1,1],[1,1,1,1,1,1,1],[1,1,1,1,1,1,1,1]],
        }
    }

    componentDidMount() {
        this.fetchStatSondaggio();

        this.willFocusSubscription = this.props.navigation.addListener(
          'willFocus',
          () => {
            this.setState({
                isLoading: true,
            }, function(){

            });
            this.fetchStatSondaggio()
          }
        );
    }

    componentWillUnmount() {
        this.willFocusSubscription.remove();
    }

    fetchStatSondaggio() {
        let city;
        let country;
        if (this.state.city == "")
            city = "null";
        if (this.state.pick_country == "0") {
            country = "null";
            city = this.state.city;
        }
        if (this.state.pick_country == "X") {
            country = "X";
            city = this.state.city;
        }
        if(this.state.pick_country == "1") {
            country = this.state.country;
            city = this.state.city;
        }
        if (this.state.city == "")
            city = "null";
        return fetch('http:/192.168.1.34:8000/api/survey/' +
            this.props.navigation.state.params.id_sondaggio + '/stats/' + this.state.question+ '/' + this.state.choice + '/' + this.state.cs
            + '/' + this.state.ed + '/' + this.state.ws + '/' + this.state.gender + '/' + country + '/' + city,{
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Token ' + global.user_key,
            }
        })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState( {
                    isLoading: false,
                    dataSource: responseJson
                }, function () {

                });
            })
            .catch((error) => {
              console.log("Error")
            })
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: height / 2}}>
                    <ActivityIndicator/>
                </View>
            )
        }
        data = this.state.dataSource;
        let tabledata = [];
        let title = [];
        let fields = [];
        if(data.stats.length>0) {
            for (let i = 0; i < data.stats.length; ++i) {
                title.push(data.stats[i].domanda);
            }
            fields = (Object.keys(data.stats[0]));
            fields.splice(fields.indexOf("domanda"), 1);
            fields.splice(fields.indexOf("totale"), 1);
            fields.splice(fields.indexOf("id"), 1);
            fields.push("totale");
            for (let i = 0; i < data.stats.length; ++i) {
                let stats = [];
                for (let j = 0; j < fields.length; ++j) {
                    let f = fields[j];
                    stats.push(data.stats[i][f]);
                }
                tabledata.push(stats)
            }
            fields.splice(0, 0, '');
        }
        return (
      <View style={styles.container}>
        <Table borderStyle={{borderWidth: 1}}>
          <Row data={fields} style={styles.head} textStyle={styles.text}/>
          <TableWrapper style={styles.wrapper}>
            <Col data={title} style={styles.tit} heightArr={[28,28]} textStyle={styles.text}/>
            <Rows data={tabledata} flexArr={this.state.flexs[fields.length-1]} style={styles.row} textStyle={styles.text}/>
          </TableWrapper>
        </Table>
      </View>
        );
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1
    },
    title: {
        fontSize: 20,
        marginVertical: 10,
        textAlign: 'center'
    },
    data: {
        paddingTop: 20,
        paddingLeft: 10
    },
    entry: {
        paddingTop: 10,
        flexDirection: 'row',
    },
    entry2: {
        paddingTop: 10,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    textTitle: {
        fontWeight: 'bold'
    },
    textData: {
        width: width - width / 3
    },
    btnText: {
        fontWeight: 'bold',
        color: 'red',
        paddingLeft: 10,


    },
    textSub: {
        fontWeight: 'bold',
        width: width - width / 3,
        color: 'red'
    },
    controlli: {
        flexDirection: 'row',
        paddingTop: 20,
        paddingRight: 28
    },
    buttonview: {
        width: 120,
        paddingRight: 5,
        paddingLeft: 5
    },
    inputContainer: {
        minWidth: '96%',
        flexDirection: 'row'
    },
    contentbar: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
      },
    leftcontainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    rightcontainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    flatlistview: {
        flex: 1,
        width: '100%',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
    head: {  height: 40,  backgroundColor: '#f1f8ff'  },
    wrapper: { flexDirection: 'row' },
    tit: { flex: 1, backgroundColor: '#f6f8fa' },
    row: {  height: 28  },
    text: { textAlign: 'center' }
});

export default SurveyStats;