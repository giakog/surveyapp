import React, { Component } from 'react';
import {View, Text, StyleSheet, Button, Image, ActivityIndicator, FlatList, TextInput} from 'react-native';
import CustomHeader from '../components/Header';
import Card from '../components/Card';
import { TouchableOpacity, TouchableWithoutFeedback, ScrollView } from 'react-native-gesture-handler';
import { RadioButton, IconButton } from 'react-native-paper';
import { Dimensions } from 'react-native';

const {width, height} = Dimensions.get('window');



class SubmissionStart extends Component {


    constructor(props){
        super(props);
        this.state ={
            error_st: false,
            error_msg: "",
            isLoading: true,
            submission_exist: false,
            creator: false,
            id_sottomissione: -1,
            id_sondaggio: this.props.navigation.state.params.id_sondaggio,
            id_creator: this.props.navigation.state.params.id_creator,
        }
    }

    componentDidMount() {
        this.fetchSottomissioneUtente();
        this.fetchDettagliSondaggio();


        this.willFocusSubscription = this.props.navigation.addListener(
          'willFocus',
          () => {
            this.setState({
                isLoading: true,
            }, function(){

            });
            this.fetchSottomissioneUtente();
            this.fetchDettagliSondaggio();

          }
        );
    }

    componentWillUnmount() {
        this.willFocusSubscription.remove();
    }
    fetchSottomissioneUtente() {
        return fetch('http:/192.168.1.34:8000/api/survey/' +
            this.props.navigation.state.params.id_sondaggio + '/my', {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Token ' + global.user_key,
            },
        })
            .then((response) => response.json())
            .then((responsejson) => {
                this.setState( {
                    submission_exist: true,
                    dataSource2: responsejson
                }, function () {

                });
            })
    }

    fetchDettagliSondaggio() {
        return fetch('http:/192.168.1.34:8000/api/survey/detail/' +
            this.props.navigation.state.params.id_sondaggio + '/submitter')
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    isLoading: false,
                    id_sondaggio: this.props.navigation.state.params.id_sondaggio,
                    creator: true,
                    dataSource: responseJson
                }, function () {
                });

            })
            .catch((error) => {
                console.log("Errore");
            });
    }

    creaRisposte = () => {
        let c;
        let q;
        let e;
        for (let i = 0; i < this.state.dataSource.questions.length; ++i) {
            q = this.state.dataSource.questions[i].id;
            for (let j = 0; j <this.state.dataSource.questions[i].choices.length; ++j) {
                if (this.state.dataSource.questions[i].choices[j].isChecked === 'checked') {
                    c = this.state.dataSource.questions[i].choices[j].id;
                    if (this.state.dataSource.questions[i].choices[j].extra_text)
                        e = this.state.dataSource.questions[i].choices[j].extra_text;
                    else
                        e = "##";

                    console.log("dati:", this.state.id_sottomissione, c, q, e);
                    if (this.state.error_st == false) {
                        fetch('http:/192.168.1.34:8000/api/submission/' + this.state.id_sottomissione + '/answers-create',
                            {
                                method: 'POST',
                                headers: {
                                    'Accept': 'application/json',
                                    'Content-Type': 'application/json',
                                    'Authorization': 'Token ' + global.user_key,
                                },
                                body: JSON.stringify({
                                    submission: this.state.id_sottomissione,
                                    choice: c,
                                    question: q,
                                    extra_text: e
                                }),
                            })
                            .then(res => res.json())
                            .then((res) => {
                                if (res.id == null) {
                                    this.setState({error_st: true});
                                    this.setState({error_message: "Errore: controlla i campi inseriti e riprova."});
                                }
                                else{
                                    console.log("id",res.id)
                                }
                            })
                            .catch((error) => {
                                console.log("Errore");
                            })
                    }
                }
            }
        }
        if(this.state.error_st == false) {
            this.props.navigation.navigate("RingraziamentoSondaggio", {messaggio_ringraziamento: this.state.dataSource.thanks_message});
        }
        else
            this.setState({error_st:"Errore"});
    };


    creaSottomissione = () => {
        let count = 0;
        for (let i = 0; i < this.state.dataSource.questions.length; ++i) {
            for (let j = 0; j <this.state.dataSource.questions[i].choices.length; ++j) {
                if(this.state.dataSource.questions[i].choices[j].isChecked ==='checked') {
                    ++count;
                }
            }
        }
        //console.log(this.state.answer_list)
        if (count != this.state.dataSource.questions.length) {
            this.setState({error_msg: "Errore: Selezionare una risposta per tutte le domande"});
        }
        if (global.user_id == -1) {
            this.setState({error_msg: "Errore: Registrazione profilo non completata"});
        }
        else if (count == this.state.dataSource.questions.length && global.user_id != -1) {
            fetch('http:/192.168.1.34:8000/api/survey/'+ this.props.navigation.state.params.id_sondaggio + '/submission-start',
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Token ' + global.user_key,
                },
              body: JSON.stringify({
                  survey: this.state.id_sondaggio,
                  is_complete: 1,
              }),
            })
                .then(res => res.json())
                .then((res) => {
                    if (res.id != null) {
                        this.setState({id_sottomissione: res.id});
                        this.creaRisposte();
                    } else {
                        this.setState({error_message: "Errore: controlla i campi inseriti e riprova."});
                    }
                })
                .catch((error) => {
                    console.log(error);
                })
        }

    };
    checkSelection(choice, object) {
        for (let i = 0; i < object.length; i++) {
          if (object[i].isChecked ) {
            object[i].isChecked = 'unchecked';
          }
        }
        choice.isChecked = 'checked';
        //console.log(this.state.choice_extra)
        this.setState({ refresh: true });
    }

    setText(choice,text, d) {
        choice.extra_text = text;
        this.setState({refresh:true});
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: height / 2}}>
                    <ActivityIndicator/>
                </View>
            )
        }
        if(this.state.submission_exist) {
            this.props.navigation.goBack(null);
        }
        data = this.state.dataSource;
        return (
            <View style={styles.screen}>

                <CustomHeader parent={this.props} />

                <View style={styles.contentbar}>
                    <View style={styles.leftcontainer}>
                        <IconButton icon="arrow-left" onPress={() => this.props.navigation.goBack(null)} />
                    </View>
                    <Text style={styles.title}>
                        {data.title}
                    </Text>
                    <View style={styles.rightcontainer}></View>
                </View>

                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{alignItems: 'center'}}>
                        <Card style={styles.inputContainer}>
                            <View style={styles.data}>
                                <View style={styles.entry}>
                                    <Text style={styles.textTitle}>Descrizione: </Text>
                                    <Text style={styles.textData}>{data.description}</Text>
                                </View>

                                <View style={styles.entry}>
                                    <Text style={styles.textTitle}>Pubblicato da: </Text>
                                    <Text style={styles.textData}>{data.creator_user.username}</Text>
                                </View>
                                <View style={{ flex: 1, padding: 20, backgroundColor: "#f2f2f2" }}>
                                    {data.questions.map((object, d) =>
                                        <View key={d} style={{ justifyContent: 'space-between' }}>
                                            <Text style={{ fontSize: 18, marginBottom: 20 }}>{object.text}</Text>
                                            {object.choices.map((choice, i) =>
                                                <View>
                                                <View key={i} style={styles.choiceCard}>
                                                    <RadioButton value={choice.id} status={choice.isChecked} onPress={() => this.checkSelection(choice, object.choices)} />
                                                    <Text style={{ fontSize: 20, paddingLeft: 10 }}>{choice.text}</Text>
                                                </View>
                                                    { object.type == 0 && object.other_field == 1 && (choice.text == "Altro" ||choice.text == 'other') && choice.isChecked ? (
                                                    <View>
                                                        <View style={styles.choiceCard}>
                                                            <TextInput style={styles.textContainer} editable maxLength={200}
                                                                       onChangeText={text =>{this.setText(choice, text,d)}}/>
                                                        </View>
                                                    </View>
                                                    ): null}
                                                </View>

                                            )}
                                        </View>
                                    )}
                                </View>
                                <View style={{paddingTop: 10}}></View>
                                <Text style={{color: 'red'}}>{this.state.error_msg}</Text>
                                <View style={{paddingTop: 10}}></View>
                                <View style={{alignItems: 'center'}}>
                                    <View style={styles.controlli}>
                                        <View style={styles.buttonview}>
                                            <Button title="Completa Sottomissione" onPress={() => this.creaSottomissione()}/>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </Card>
                    </View>
                </ScrollView>
            </View>
        );
    }

}
const styles = StyleSheet.create({
    screen: {
        flex: 1
    },
    title: {
        fontSize: 20,
        marginVertical: 10,
        textAlign: 'center'
    },
    data: {
        paddingTop: 20,
        paddingLeft: 10
    },
    entry: {
        paddingTop: 10,
        flexDirection: 'row',
    },
    entry2: {
        paddingTop: 10,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    textTitle: {
        fontWeight: 'bold'
    },
    textContainer: {
        borderWidth: 1,
        height: 28,
        width: width - width / 2,
        marginLeft: 10,
        marginBottom: 3,
        marginTop: 3
    },
    textData: {
        width: width - width / 3
    },
    btnText: {
        fontWeight: 'bold',
        color: 'red',
        paddingLeft: 10,
    },
    textSub: {
        fontWeight: 'bold',
        width: width - width / 3,
        color: 'red'
    },
    controlli: {
        flexDirection: 'row',
        paddingTop: 20,
        paddingRight: 28
    },
    buttonview: {
        width: 150,
        paddingRight: 5,
        paddingLeft: 5,
        marginBottom: 10
    },
    inputContainer: {
        minWidth: '96%',
        flexDirection: 'row'
    },
    contentbar: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
      },
    leftcontainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    rightcontainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    flatlistview: {
        flex: 1,
        width: '100%',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    choiceCard: {
        paddingLeft: 6,
        alignItems: 'center',
        flexDirection: 'row',
        marginBottom: 16,
        backgroundColor: 'white',
        height: 55,
        elevation: 1,
        borderRadius: 4,
    }
});

export default SubmissionStart;