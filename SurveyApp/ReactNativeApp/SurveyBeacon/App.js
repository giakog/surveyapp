import React, { Component, createContext } from 'react';
import { createAppContainer } from 'react-navigation';
import * as Font from 'expo-font';
import { View, Text, StyleSheet, Button, Image, TextInput, ScrollView, TouchableOpacity } from 'react-native';
const {width, height} = Dimensions.get('window');
import { Dimensions } from 'react-native';
import Drawer from './components/DrawerNavigator';


const AppContainer = createAppContainer(Drawer);


export default class App extends React.Component{
    constructor() {
        super();
        global.logged_in = false;
        global.user_key = 1;
        global.username = "";
        global.user_id = "-1";
        global.cs = "";
        global.ed = "";
        global.gender = "";
        global.ws = "";
        global.country = "";
        global.city = "";
        global.category = "All";
        global.category_list = [];
        global.tag_list = [];
    }
    state = {
        fontLoaded: false,
        isLoading: true,
        show_pickers: true
    };

    async componentDidMount() {
        await Font.loadAsync({
          'satisfy': require('./assets/fonts/satisfy.ttf'),
          'typold-medium': require('./assets/fonts/typold-medium.otf')
        });
        this.setState({ fontLoaded: true });
    }
    render () {
        if (!this.state.fontLoaded) {
          return (
            <View></View>
          );
        }
    return (
      <AppContainer navigation={this.props.navigation}/>
    );
  }
}


