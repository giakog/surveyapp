from django.conf import settings
from django.db import models
from django.shortcuts import reverse
from django_countries.fields import CountryField
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.contrib.auth.models import User

# Create your models here.


STATUS_CHOICES = (
    ('0', 'Inattivo'),
    ('1', 'Attivo'),
    ('2', 'Bloccato'),
)

SEX_CHOICES = (
    ('maschio', 'Maschio'),
    ('femmina', 'Femmina'),
    ('altro', 'Altro'),
)

CIVIL_STATE = (
    ('single', 'Single'),
    ('sposato/a', 'Sposato/a'),
    ('vedovo/a', 'Vedovo/a'),
    ('divorziato/a', 'Divorziato/a')
)

EDUCATION = (
    ('elementare', 'Licenza Elementare'),
    ('media', 'Licenza Media'),
    ('diploma', 'Diploma'),
    ('laurea', 'Laurea')
)

WORKING_STATE = (
    ('studente', 'Studente'),
    ('disoccupato', 'Disoccupato'),
    ('occupato', 'Occupato'),
    ('ritirato', 'Ritirato')
)


TYPE_CHOICES = (
    ('0', 'Risposta Multipla'),
    ('1', 'Voto 1-5'),
)


class Tag(models.Model):
    title = models.CharField(max_length=100)

    def __str__(self):
        return self.title


class Category(models.Model):
    title = models.CharField(max_length=100)
    slug = models.SlugField(default="default-category")
    image = models.ImageField(upload_to='category/', default='category/noimage.jpg')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("polls:survey-list", kwargs={
            'slug': self.slug
        })


class Survey(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField()
    thanks_message = models.TextField(default="")
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    creator = models.ForeignKey(User, on_delete=models.CASCADE)
    tags = models.ManyToManyField(Tag)
    slug = models.SlugField()
    date = models.DateTimeField(auto_now_add=True)
    status = models.CharField(choices=STATUS_CHOICES, max_length=1)
    subs = models.IntegerField(default=0)

    def __str__(self):
        return self.title


class Question(models.Model):
    text = models.TextField()
    survey = models.ForeignKey(Survey, related_name='questions', on_delete=models.CASCADE)
    type = models.CharField(choices=TYPE_CHOICES, max_length=1, default=0)
    other_field = models.BooleanField()

    def __str__(self):
        return self.text


class Choice(models.Model):
    survey = models.ForeignKey(Survey, related_name='choices', on_delete=models.CASCADE)
    question = models.ForeignKey(Question, related_name='choices', on_delete=models.CASCADE)
    text = models.CharField(max_length=200)

    def __str__(self):
        return str(self.id)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    civil_state = models.CharField(choices=CIVIL_STATE, max_length=10)
    education = models.CharField(choices=EDUCATION, max_length=10)
    working_state = models.CharField(choices=WORKING_STATE, max_length=10)
    sex = models.CharField(choices=SEX_CHOICES, max_length=10)
    country = CountryField(multiple=False)
    city = models.CharField(max_length=100)

    def __str__(self):
        return self.user.username


class Submission(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    survey = models.ForeignKey(Survey, related_name='submissions', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    is_complete = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username


class Answer(models.Model):

    submission = models.ForeignKey(Submission, related_name='answers', on_delete=models.CASCADE)
    choice = models.ForeignKey(Choice, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    extra_text = models.CharField(max_length=200, default="")

    def __str__(self):
        return self.choice.text

