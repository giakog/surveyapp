from django.urls import path
from .views import HomeView, survey_list, CreateSurveyView, edit, question_create, choice_create, detail, start, \
    submit, thanks, EditProfileView, filter_surveys_by_input, filter_surveys_by_tag, ProfileView, view_submission, \
    add_other, survey_detail_stats, load_choices, delete_stop, survey_delete, survey_stop, my_surveys, my_submissions, \
    choice_delete, choice_edit, question_delete, question_edit, survey_info_edit

app_name = 'polls'

urlpatterns = [
    path("", HomeView.as_view(), name='home'),
    path('profile/', ProfileView.as_view(), name='profile'),
    path('profile/my-surveys', my_surveys, name='my-surveys'),
    path('profile/my-submissions', my_submissions, name='my-submissions'),
    path('profile-edit/', EditProfileView.as_view(), name='profile-edit'),
    path('survey-list/<slug>', survey_list, name='survey-list'),
    path('survey-list/<slug>/search/', filter_surveys_by_input, name='survey-filter-input'),
    path('survey-list/<slug>/tag=<tag>/', filter_surveys_by_tag, name='survey-filter-tag'),
    path('surveys/create/', CreateSurveyView.as_view(), name='survey-create'),
    path('surveys/<int:pk>/edit', edit, name='survey-edit'),
    path('surveys/<int:pk>/edit-info', survey_info_edit, name='survey-edit-info'),
    path('surveys/<int:pk>/delete-stop', delete_stop, name='survey-delete-stop'),
    path('surveys/<int:pk>/delete', survey_delete, name='survey-delete'),
    path("surveys/<int:pk>/delete", survey_delete, name='survey-delete'),
    path("surveys/<int:pk>/stop", survey_stop, name='survey-stop'),
    path("surveys/<int:pk>/", detail, name="survey-detail"),
    path("surveys/<int:pk>/question/", question_create, name="question-create"),
    path("surveys/<int:pk>/question/<int:question_pk>/delete", question_delete, name="question-delete"),
    path("surveys/<int:pk>/question/<int:question_pk>/edit", question_edit, name="question-edit"),
    path("surveys/<int:survey_pk>/question/<int:question_pk>/choice/", choice_create, name="choice-create"),
    path("surveys/<int:survey_pk>/question/<int:question_pk>/add-other/", add_other, name='survey-edit-other'),
    path("surveys/<int:survey_pk>/question/<int:question_pk>/choice/<int:choice_pk>/delete", choice_delete,
         name="choice-delete"),
    path("surveys/<int:survey_pk>/question/<int:question_pk>/choice/<int:choice_pk>/edit", choice_edit,
         name="choice-edit"),
    path("surveys/<int:pk>/start/", start, name="survey-start"),
    path("surveys/<int:survey_pk>/submit/<int:sub_pk>/", submit, name="survey-submit"),
    path("surveys/<int:survey_pk>/view-submission/<int:sub_pk>/", view_submission, name="submission-view"),
    path("surveys/<int:pk>/thanks/", thanks, name="survey-thanks"),
    path("surveys/<int:pk>/stats/#<str:sf>", survey_detail_stats, name="survey-stats"),
    path('ajax/load-choices/<int:pk>', load_choices, name='ajax_load_choices'), # AJAX

]
