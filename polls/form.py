from django import forms
from django_countries.fields import CountryField
from django_countries.widgets import CountrySelectWidget
from .models import Category, Tag, Question, Choice
from django.db.models import Q


SEX_CHOICES = (
    ('maschio', 'Maschio'),
    ('femmina', 'Femmina'),
    ('altro', 'Altro'),
)

CIVIL_STATE = (
    ('single', 'Single'),
    ('sposato/a', 'Sposato/a'),
    ('vedovo/a', 'Vedovo/a'),
    ('divorziato/a', 'Divorziato/a')
)

EDUCATION = (
    ('elementare', 'Licenza Elementare'),
    ('media', 'Licenza Media'),
    ('diploma', 'Diploma'),
    ('laurea', 'Laurea')
)

WORKING_STATE = (
    ('studente', 'Studente'),
    ('disoccupato', 'Disoccupato'),
    ('occupato', 'Occupato'),
    ('ritirato', 'Ritirato')
)


class EditProfileForm(forms.Form):
    civil_state = forms.ChoiceField(label='Stato civile', choices=CIVIL_STATE)
    education = forms.ChoiceField(label='Educazione', choices=EDUCATION)
    working_state = forms.ChoiceField(label='Stato lavorativo', choices=WORKING_STATE)
    sex = forms.ChoiceField(label='Genere', widget=forms.RadioSelect, choices=SEX_CHOICES)
    paese = CountryField(blank_label='(Selezione Paese)').formfield(widget=CountrySelectWidget(attrs={
        'class': 'custom-select d-block w-50'
    }))
    city = forms.CharField(max_length=100, label='Citta', required=True,
                           widget=forms.TextInput(attrs={'class': 'd-block w-50'}))


class CreateSurveyForm(forms.Form):
    title = forms.CharField(max_length=200, label='Titolo', required=True,
                            widget=forms.TextInput())
    description = forms.CharField(widget=forms.Textarea(
        attrs={'rows': "5", 'cols': "50", 'class': 'vLargeTextField'})
    )
    thanks_message = forms.CharField(label='Messaggio di ringraziamento', widget=forms.Textarea(
        attrs={'rows': "5", 'cols': "50", 'class': 'vLargeTextField'})
    )
    category = forms.ModelChoiceField(label='Categoria', queryset=Category.objects.filter(~Q(title="All")))
    tags = forms.ModelMultipleChoiceField(label='Tags', widget=forms.CheckboxSelectMultiple, queryset=Tag.objects.all())


class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ["text", "type", "other_field"]
        labels = {"text": "Testo", "type": "Tipo", "other_field": "Campo altro"}


class ChoiceForm(forms.ModelForm):
    class Meta:
        model = Choice
        fields = ["text"]
        labels = {"text": "Testo"}


class VoteForm(forms.ModelForm):
    required_css_class = 'required'
    vote_select = (('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'))
    vote = forms.ChoiceField(label="Voto", choices=vote_select)


class OtherChoiceForm(forms.Form):
    text = forms.CharField(label="text", required=True, max_length=200, widget=forms.TextInput(attrs={'class': 'd-block w-50'}))


class AnswerForm(forms.Form):
    def __init__(self, *args, **kwargs):
        choices = kwargs.pop("choice")
        print(choices)

        choice = {(o.pk, o.text) for o in choices}
        super().__init__(*args, **kwargs)
        choice_field = forms.ChoiceField(choices=sorted(choice), widget=forms.RadioSelect, required=True)
        self.fields["choice"] = choice_field


class AnswerFormSave(forms.Form):
    def __init__(self, *args, **kwargs):
        choices = kwargs.pop("choice")
        choice = {(o.pk, o.text) for o in choices}
        super().__init__(*args, **kwargs)
        choice_field = forms.ChoiceField(choices=choice, widget=forms.RadioSelect, required=False)
        self.fields["choice"] = choice_field


class BaseAnswerFormSet(forms.BaseFormSet):
    def get_form_kwargs(self, index):
        kwargs = super().get_form_kwargs(index)
        kwargs["choice"] = kwargs["choice"][index]
        return kwargs


class CustomStatsForm(forms.Form):

    def __init__(self, *args, **kwargs):
        sur_pk = kwargs.pop("form_kwargs")
        super().__init__(*args, **kwargs)

        qst = [(o.pk, str(o)) for o in Question.objects.filter(survey_id=sur_pk)]
        if 'question' in self.data:
            try:
                q_id = int(self.data.get('question'))
                options = [(o.id, str(o.text)) for o in Choice.objects.filter(question_id=q_id)]
                print(options)
            except (ValueError, TypeError):
                pass
        else:
            options = [(o.id, str(o.text)) for o in Choice.objects.filter(survey_id=sur_pk)]
        choices_1 = [('0', 'Non Selezionato')] + qst
        choices_2 = [('0', 'Non Selezionato')] + options
        gender = (('0', 'Non Selezionato'), ('X', 'Tutti'), ('maschio', 'Maschio'), ('femmina', 'Femmina'), ('altro', 'Altro'))
        cs = (('0', 'Non selezionato'), ('X', 'Tutti'), ('single', 'Single'), ('sposato/a', 'Sposato/a'), ('vedovo/a', 'Vedovo/a'),
              ('divorziato/a', 'Divorziato/a'))
        ed = (('0', 'Non Selezionato'), ('X', 'Tutti'), ('elementare', 'Licenza Elementare'), ('media', 'Licenza Media'),
              ('diploma', 'Diploma'), ('laurea', 'Laurea'))
        ws = (('0', 'Non Selezionato'), ('X', 'Tutti'), ('studente', 'Studente'), ('disoccupato', 'Disoccupato'), ('occupato', 'Occupato'),
              ('ritirato', 'Ritirato'))
        print(self.fields)
        self.fields["question"] = forms.ChoiceField(label='Domanda', choices=choices_1, required=True)
        self.fields['choice'] = forms.ChoiceField(label='Opzione',choices=choices_2, required=True)
        self.fields["civil_state"] = forms.ChoiceField(label='Stato civile', choices=cs, required=False)
        self.fields["education"] = forms.ChoiceField(label='Educazione', choices=ed, required=False)
        self.fields["working_state"] = forms.ChoiceField(label='Stato lavorativo', choices=ws, required=False)
        self.fields["gender"] = forms.ChoiceField(label='Genere', choices=gender, required=False)
        self.fields["paese"] = CountryField(blank=True, blank_label='Non Selezionato')\
            .formfield(widget=CountrySelectWidget())
        self.fields["city"] = forms.CharField(label='Citta', max_length=100, required=False,
                                              widget=forms.TextInput())


