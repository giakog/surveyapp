from django.urls import reverse
from django.test import TestCase, Client
from django.contrib.auth.models import User
from polls.models import Profile
from polls.models import Survey, Question, Choice, Submission, Answer
import pytest


@pytest.mark.django_db
class TestView(TestCase):
    def setUp(self):
        self.user_login = Client()
        self.user = User.objects.create_user(username='usr', email='usr@mail.com', password='12345')
        self.profile = Profile.objects.get(user=self.user.id)
        self.profile.civil_state = 'single'
        self.profile.working_state = 'studente'
        self.profile.sex = 'maschio'
        self.profile.education = 'elementare'
        self.profile.country = 'IT'
        self.profile.city = 'Modena'
        self.profile.save()
        self.user_login.login(username='usr', password='12345')

        self.survey = Survey.objects.create(
            creator=self.user.id,
            title='Titolo',
            description='Descrizione',
            thanks_message='Grazie',
            category=1,
            tags=[1, 2],
            slug='Title',
            status=1
        )
        self.empty_survey = Survey.objects.create(
            creator=self.user.id,
            title='Titolo',
            description='Descrizione',
            thanks_message='Grazie',
            category=1,
            tags=[1, 2],
            slug='Title',
            status=0
        )
        self.empty_question =Question.objects.create(
            survey_id=self.empty_survey.id,
            text='Testo',
            type=0,
            other_field=False
        )
        question = Question.objects.create(
            survey_id=self.survey.id,
            text='Testo',
            type=0,
            other_field=False
        )
        choice = Choice.objects.create(
            question_id=question.id,
            text='Testo'
        )
        self.submission = Submission.objects.create(
            user=self.user.id,
            is_complete=True,
            survey_id=self.survey.id,
        )
        self.empty_submission = Submission.objects.create(
            user=self.user.id,
            is_complete=False,
            survey_id=self.empty_survey.id,
        )

    def survey_user_authenticated(self):
        path = reverse('polls:my-surveys')
        response = self.user_login.get(path)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'my_surveys.html')

    def submission_user_authenticated(self):
        path = reverse('polls:my-submissions')
        response = self.user_login.get(path)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'my_submissions.html')

    def submission_detail_user_authenticated(self):
        path = reverse('polls:submission-view', kwargs={'survey_pk': self.survey.id, 'sub_pk': self.submission.id})
        response = self.user_login.get(path)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'submission_detail.html')

    def survey_detail_user_authenticated(self):
        path = reverse('polls:survey-detail', kwargs={'pk': self.survey.id})
        response = self.user_login.get(path)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'detail.html')

    def survey_new_user_authenticated(self):
        path = reverse('polls:survey-create', )
        response = self.user_login.get(path)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'new_survey.html')

    def survey_edit_user_authenticated(self):
        path = reverse('polls:survey-detail', kwargs={'pk': self.empty_survey.id})
        response = self.user_login.get(path)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'survey_edit.html')

    def survey_question_create_user_authenticated(self):
        path = reverse('polls:question-create', kwargs={'pk': self.empty_survey.id})
        response = self.user_login.get(path)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'question.html')

    def survey_choice_user_authenticated(self):
        path = reverse('polls:choice-create', kwargs={'survey_pk': self.empty_survey.id,
                                                      'question_pk': self.empty_question.id})
        response = self.user_login.get(path)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'choice.html')

    def submission_submit_user_authenticated(self):
        path = reverse('polls:choice-create', kwargs={'survey_pk': self.empty_survey.id,
                                                      'sub_pk': self.empty_submission.id})
        response = self.user_login.get(path)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'submit.html')

