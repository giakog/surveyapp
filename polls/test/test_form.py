from django.test import TestCase
from polls.form import *


class TestForm(TestCase):

    def test_survey_form_valid(self):
        form = CreateSurveyForm(data={
            'title': 'Titolo',
            'description': 'Descrizione',
            'thanks_message': 'Grazie',
            'category': 1,
            'tags': [1, 2]
        })
        self.assertTrue(form.is_valid())

    def test_survey_form_empty(self):
        form = CreateSurveyForm({})
        self.assertFalse(form.is_valid())
        self.assertEquals(len(form.errors), 5)

    def test_question_form_valid(self):
        form = QuestionForm(data={
            'text': 'Testo',
            'type': 0,
            'other_field': False
        })
        self.assertTrue(form.is_valid())

    def test_question_form_empty(self):
        form = QuestionForm(data={})
        self.assertFalse(form.is_valid())
        self.assertEquals(len(form.errors), 3)

    def test_option_form_valid(self):
        form = ChoiceForm(data={
            'text': 'Test'
        })
        self.assertTrue(form.is_valid())

    def test_option_form_empty(self):
        form = ChoiceForm(data={})
        self.assertFalse(form.is_valid())
        self.assertEquals(len(form.errors), 1)
