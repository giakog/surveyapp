from django.contrib.staticfiles.templatetags.staticfiles import static
from django.test import TestCase
from polls.models import Profile, Survey, Question, Choice, Submission, Answer
from django.contrib.auth.models import User


class TestModel(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='user', email='user@mail.com', password='12345')

        Profile.objects.create(user=self.user.id, civil_state='single', working_state='studente',
                               sex='maschio', education='elementare', country='IT', city='Modena')

        survey = Survey.objects.create(creator=self.user.id, title='Titolo', description='Descrizione',
                                       thanks_message='Grazie', category=1, status=1, slug='Titolo', tags=[1, 2])

        question = Question.objects.create(survey_id=survey.pk, text='Testo', type=0, other_field=False)

        choice = Choice.objects.create(survey_id=survey.pk, question_id=question.id, text='Testo')

        submission = Submission.objects.create(user_id=self.user.id, survey_id=survey.pk, is_complete=True)

        answer = Answer.objects.create(submission_id=submission.id, question_id=question.id,
                                       choice_id=choice.id)

