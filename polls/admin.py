from django.contrib import admin
from .models import Tag, Category, Survey, Question, Choice, Answer, Profile, Submission

# Register your models here.


admin.site.register(Profile)
admin.site.register(Tag)
admin.site.register(Category)
admin.site.register(Survey)
admin.site.register(Question)
admin.site.register(Choice)
admin.site.register(Submission)
admin.site.register(Answer)
