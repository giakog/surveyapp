from django.shortcuts import get_object_or_404
from django.db import transaction
from django.db.models import Q, Count
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.contrib import messages
from .models import Category, Survey, Tag, Profile, Question, Choice, Answer, Submission, User
from .form import CreateSurveyForm, QuestionForm, ChoiceForm, AnswerForm, AnswerFormSave, \
                    BaseAnswerFormSet, EditProfileForm, OtherChoiceForm, CustomStatsForm
from django.forms.formsets import formset_factory
from django.http import Http404
from django.shortcuts import render, redirect
from django.utils import timezone
from django.views.generic import ListView, View
from django.contrib.auth.decorators import login_required

# Create your views here.


# AJAX
def load_choices(request, pk):
    question_id = request.GET.get('question')
    choices = Choice.objects.filter(question_id=question_id).all()
    if len(choices) == 0:
        choices = Choice.objects.filter(survey_id=pk).all()
    return render(request, 'choice_filter.html', {'choices': choices})


class HomeView(ListView):
    template_name = "home.html"
    model = Category
    paginate_by = 10


class ProfileView(View):

    def get(self, *args, **kwargs):
        try:
            profile = Profile.objects.get(user=self.request.user)
            surveys = Survey.objects.filter(creator=self.request.user).annotate(count=Count('submissions')).order_by('-count')[:5]
            surveys_number = Survey.objects.filter(creator=self.request.user).count()
            subs = Submission.objects.filter(user=self.request.user).order_by('-created_at')[:5]
            total_sub = 0
            for survey in surveys:
                total_sub += survey.subs
            context = {
                'profile': profile,
                'surveys_number': surveys_number,
                'surveys': surveys,
                'submissions': subs,
                'total_submissions': total_sub,
            }
            return render(self.request, "profile.html", context)
        except ObjectDoesNotExist:
            if self.request.user:
                return redirect("polls:profile-edit")
            else:
                messages.info(self.request, "Error loading profile")
                return redirect("polls:home")


@login_required
def my_surveys(request):
    surveys = Survey.objects.filter(creator=request.user).annotate(count=Count('submissions')).order_by('-date')
    context = {
        'surveys': surveys,
    }
    return render(request, "my_surveys.html", context)


@login_required
def my_submissions(request):
    subs = Submission.objects.filter(user=request.user).order_by('-created_at')
    context = {
        'submissions': subs,
    }
    return render(request, "my_submissions.html", context)


def get_page(request, surveys):
    page = request.GET.get('page', 1)
    paginator = Paginator(surveys, 20)
    try:
        surveys_per_pag = paginator.page(page)
    except PageNotAnInteger:
        surveys_per_pag = paginator.page(1)
    except EmptyPage:
        surveys_per_pag = paginator.page(paginator.num_pages)
    return surveys_per_pag


def survey_list(request, slug):
    category = get_object_or_404(Category, slug=slug)
    top_tags = Tag.objects.annotate(count=Count('survey')).order_by('-count')[:5]
    if str(category) == "All":
        surveys = Survey.objects.filter(status=1)
    else:
        surveys = Survey.objects.filter(category=category, status=1)
    surveys_per_pag = get_page(request, surveys)
    context = {
        'category': category,
        'surveys': surveys_per_pag,
        'top_tags': top_tags,
        'active': "All",
    }
    return render(request, 'survey_list.html', context)


def filter_surveys_by_input(request, slug):
    category = get_object_or_404(Category, slug=slug)
    top_tags = Tag.objects.annotate(count=Count('survey')).order_by('-count')[:5]
    print(top_tags)
    query = request.GET.get('q')
    if str(category) == "All":
        surveys = Survey.objects.filter(Q(status__exact=1) & (Q(title__contains=query) |
                                                              Q(description__contains=query)))
    else:
        surveys = Survey.objects.filter(Q(category__exact=category) & Q(status__exact=1) &
                                        (Q(title__contains=query) | Q(description__contains=query)))
    surveys_per_pag = get_page(request, surveys)
    context = {
        'category': category,
        'surveys': surveys_per_pag,
        'top_tags': top_tags,
        'active': "All",
    }
    return render(request, 'survey_list.html', context)


def filter_surveys_by_tag(request, slug, tag):
    category = get_object_or_404(Category, slug=slug)
    top_tags = Tag.objects.annotate(count=Count('survey')).order_by('-count')[:5]
    print(top_tags)
    if str(category) == "All":
        surveys = Survey.objects.filter(tags__title__icontains=tag, status=1)
    else:
        surveys = Survey.objects.filter(tags__title__icontains=tag, category=category, status=1)
    surveys_per_pag = get_page(request, surveys)
    context = {
        'category': category,
        'surveys': surveys_per_pag,
        'top_tags': top_tags,
        'active': tag,
    }
    return render(request, 'survey_list.html', context)


class EditProfileView(View):
    def get(self, *args, **kwargs):
        profile = Profile.objects.filter(user=self.request.user)
        if profile.exists():
            form = EditProfileForm(initial={
                "civil_state": profile[0].civil_state,
                "education": profile[0].education,
                "working_state": profile[0].working_state,
                "sex": profile[0].sex,
                "paese": profile[0].country,
                "city": profile[0].city,
            })
        else:
            form = EditProfileForm()
        context = {
            'form': form,
        }
        return render(self.request, 'profile_edit.html', context)

    def post(self, *args, **kwargs):
        profile = Profile.objects.update_or_create(user=self.request.user)
        form = EditProfileForm(self.request.POST or None)
        if form.is_valid():
            civil_state = form.cleaned_data.get('civil_state')
            education = form.cleaned_data.get('education')
            working_state = form.cleaned_data.get('working_state')
            sex = form.cleaned_data.get('sex')
            country = form.cleaned_data.get('paese')
            city = form.cleaned_data.get('city')
            profile[0].user = self.request.user
            profile[0].civil_state = civil_state
            profile[0].education = education
            profile[0].working_state = working_state
            profile[0].sex = sex
            profile[0].country = country
            profile[0].city = city
            profile[0].save()
            return redirect('polls:profile')
        return render(self.request, 'profile_edit.html', {"form": form})


class CreateSurveyView(View):

    def get(self, *args, **kwargs):
        try:
            form = CreateSurveyForm()
            context = {
                'form': form,
            }
            return render(self.request, 'new_survey.html', context)
        except ObjectDoesNotExist:
            messages.info(self.request, "ERRORE: Errore caricamento pagina")
            return redirect("/")

    def post(self, *args, **kwargs):
        n_s = Survey.objects.all().count()
        try:
            form = CreateSurveyForm(self.request.POST or None)
            if form.is_valid():
                title = form.cleaned_data.get('title')
                description = form.cleaned_data.get('description')
                thanks_message = form.cleaned_data.get('thanks_message')
                category = form.cleaned_data.get('category')
                print("ok")
                tags = form.cleaned_data.get('tags')
                print(tags)
                slug = title.strip() + str(n_s)
                survey = Survey.objects.create(
                    title=title,
                    description=description,
                    thanks_message=thanks_message,
                    category=category,
                    creator=self.request.user,
                    slug=slug,
                    date=timezone.now(),
                    status=0
                )
                if tags:
                    for tag in tags:
                        survey.tags.add(tag)
                    survey.save()
                return redirect('polls:survey-edit', pk=survey.id)
        except ObjectDoesNotExist:
            form = CreateSurveyForm()
            print("exception")
            messages.error(self.request, "ERRORE: Si è verificato un errore")
        return render(self.request, 'new_survey.html', {"form": form})


@login_required
def survey_info_edit(request, pk):
    try:
        survey = Survey.objects.get(id=pk, creator=request.user)
    except Survey.DoesNotExist:
        raise Http404()
    if request.method == 'POST':
        form = CreateSurveyForm(request.POST)
        if form.is_valid():
            survey.title = form.cleaned_data.get('title')
            survey.category = form.cleaned_data.get('category')
            survey.description = form.cleaned_data.get('description')
            survey.thanks_message = form.cleaned_data.get('thanks_message')
            new_tags = form.cleaned_data.get('tags')
            survey.tags.clear()
            for tag in new_tags:
                survey.tags.add(tag)
            survey.save()
            messages.info(request, "Sondaggio modificato!")
            return redirect('polls:survey-edit', pk=pk)
    else:
        form = CreateSurveyForm(initial={
            "title": survey.title,
            "description": survey.description,
            "category": survey.category,
            "thanks_message": survey.thanks_message,
            "tags": [tag for tag in survey.tags.values_list('id', flat=True)]
        })
    return render(request, "new_survey.html", {"form": form})


@login_required
def survey_info_edit(request, pk):
    try:
        survey = Survey.objects.get(id=pk, creator=request.user)
    except Survey.DoesNotExist:
        raise Http404()
    survey.delete()

    return redirect("/")


@login_required
def detail(request, pk):
    """User can view an active survey"""
    try:
        survey = Survey.objects.prefetch_related("questions__choices").get(
            Q(pk__exact=pk) & Q(creator__exact=request.user) & (Q(status__exact=1) | Q(status__exact=2))
        )
    except Survey.DoesNotExist:
        raise Http404()

    qs = survey.questions.all()
    if request.POST:
        stats = []
        form = CustomStatsForm(request.POST, form_kwargs=survey.pk)
        print(form.is_valid())
        if form.is_valid():
            question = form.cleaned_data.get('question')
            stats.append(question)
            choice = form.cleaned_data.get('choice')
            stats.append(choice)
            cs = form.cleaned_data.get('civil_state')
            stats.append(cs)
            ed = form.cleaned_data.get('education')
            stats.append(ed)
            ws = form.cleaned_data.get('working_state')
            stats.append(ws)
            gender = form.cleaned_data.get('gender')
            stats.append(gender)
            country = form.cleaned_data.get('paese')
            stats.append(country)
            city = form.cleaned_data.get('city')
            stats.append(city)
            return redirect('polls:survey-stats', survey.id, stats)

    # Calculate the results.
    # This is a naive implementation and it could be optimised to hit the database less.
    # See here for more info on how you might improve this code: https://docs.djangoproject.com/en/3.1/topics/db/aggregation/
    else:
        form = CustomStatsForm(form_kwargs=survey.pk)
        for question in qs:
            option_pks = question.choices.values_list("pk", flat=True)
            total_answers = Answer.objects.filter(choice_id__in=option_pks, submission__is_complete=True).count()
            for choice in question.choices.all():
                num_answers = Answer.objects.filter(choice=choice, submission__is_complete=True).count()
                choice.percent = 100.0 * num_answers / total_answers if total_answers else 0
                choice.answer_per_choice = num_answers if total_answers else 0

    description = survey.description
    num_subs = survey.submissions.filter(is_complete=True).count()
    return render(
        request,
        "detail.html",
        {
            "survey": survey,
            "description": description,
            "questions": qs,
            "num_submissions": num_subs,
            "form": form,
        },
    )


def get_filtered_profiles(prof_1, f, i, c_id):
    if i == 2:
        prof_1 = prof_1.prefetch_related('user__submissions__answers') \
            .filter(civil_state=f[i],
                    user__submission__answers__in=Answer.objects.filter(choice_id=c_id))
    if i == 3:
        prof_1 = prof_1.prefetch_related('user__submissions__answers') \
            .filter(education=f[i],
                    user__submission__answers__in=Answer.objects.filter(choice_id=c_id))
    if i == 4:
        prof_1 = prof_1.prefetch_related('user__submissions__answers') \
            .filter(working_state=f[i],
                    user__submission__answers__in=Answer.objects.filter(choice_id=c_id))
    if i == 5:
        prof_1 = prof_1.prefetch_related('user__submissions_answers') \
            .filter(sex=f[i],
                    user__submission__answers__in=Answer.objects.filter(choice_id=c_id))
    if i == 6:
        print(f[i])
        if f[i] != "ZZ":
            prof_1 = prof_1.prefetch_related('user__submissions__answers') \
                .filter(country=f[i],
                        user__submission__answers__in=Answer.objects.filter(choice_id=c_id))
    if i == 7:
        print(f[i])
        if f[i] != "all" and f[i] != "All" and f[i] != "ALL":
            prof_1 = prof_1.prefetch_related('user__submissions_answers') \
                .filter(city=f[i],
                        user__submission__answers__in=Answer.objects.filter(choice_id=c_id))
    return prof_1


def group_profiles(prof, indx, grp, f, c_id):
    prof_1 = prof
    tot = 0
    txt = []
    for i in indx:
        if i != 0 and i != 1 and f[i] != "X":
            prof_1 = get_filtered_profiles(prof_1, f, i, c_id)

    if len(grp) == 1:
        tot = prof_1.values(grp[0]).annotate(totale=Count(grp[0])).order_by(grp[0])
    elif len(grp) == 2:
        tot = prof_1.values(grp[0], grp[1]).annotate(totale=Count(grp[0])).order_by(grp[0],grp[1])
    elif len(grp) == 3:
        tot = prof_1.values(grp[0], grp[1], grp[2]).annotate(totale=Count(grp[0])).order_by(grp[0], grp[1], grp[2])
    elif len(grp) == 4:
        tot = prof_1.values(grp[0], grp[1], grp[2], grp[3]).\
            annotate(totale=Count(grp[0])).order_by(grp[0], grp[1], grp[2], grp[3])
    elif len(grp) == 5:
        tot = prof_1.values(grp[0], grp[1], grp[2], grp[3], grp[4]). \
            annotate(totale=Count(grp[0])).order_by(grp[0], grp[1], grp[2], grp[3], grp[4])
    elif len(grp) == 6:
        tot = prof_1.values(grp[0], grp[1], grp[2], grp[3], grp[4], grp[5]). \
            annotate(totale=Count(grp[0])).order_by(grp[0], grp[1], grp[2], grp[3], grp[4], grp[5])
    elif len(grp) == 7:
        tot = prof_1.values(grp[0], grp[1], grp[2], grp[3], grp[4], grp[5], grp[6]). \
            annotate(totale=Count(grp[0])).order_by(grp[0], grp[1], grp[2], grp[3], grp[4], grp[5], grp[6])
    elif len(grp) == 8:
        tot = prof_1.values(grp[0], grp[1], grp[2], grp[3], grp[4], grp[5], grp[6], grp[7]). \
            annotate(totale=Count(grp[0])).order_by(grp[0], grp[1], grp[2], grp[3], grp[4], grp[5], grp[6], grp[7])
    return tot


def main_survey_loop(indx, opt, questions, grp, f):
    tot_ret = []
    if opt == 2:
        for question in questions:
            if f[1] != "0":
                choice = Choice.objects.get(question_id=question.id, id=int(f[1]))
                prof = Profile.objects.prefetch_related('user__submissions__answers') \
                    .filter(user__submission__answers__in=Answer.objects.filter(question_id=question.id, choice_id=int(f[1])))
                tot = group_profiles(prof, indx, grp, f, int(f[1]))
                for t in tot:
                    t['Question'] = question.text
                    t['Choice'] = choice.text
                    t['id'] = choice.id
                    tot_ret.append(t)
            else:
                for choice in question.choices.all():
                    prof = Profile.objects.prefetch_related('user__submissions__answers') \
                        .filter(user__submission__answers__in=Answer.objects.filter(question_id=question.id,
                                                                                     choice_id=choice.id))
                    tot = group_profiles(prof, indx, grp, f, choice.id)
                    for t in tot:
                        t['Question'] = question.text
                        t['Choice'] = choice.text
                        t['id'] = choice.id
                        tot_ret.append(t)
    else:
        if f[1] != "0":
            choice = Choice.objects.get(question_id=questions.id, id=int(f[1]))
            prof = Profile.objects.prefetch_related('user__submission_set__answer_set') \
                .filter(
                user__submission__answers__in=Answer.objects.filter(question_id=questions.id, choice_id=int(f[1])))
            tot = group_profiles(prof, indx, grp, f, int(f[1]))
            print("choice:", choice.text, tot)
            if tot != 0:
                for t in tot:
                    t['Question'] = questions.text
                    t['Choice'] = choice.text
                    t['id'] = choice.id
                    tot_ret.append(t)

        else:
            for choice in questions.choices.all():
                prof = Profile.objects.prefetch_related('user__submissions__answers') \
                    .filter(user__submission__answers__in=Answer.objects.filter(question_id=questions.id,
                                                                               choice_id=choice.id))
                tot = group_profiles(prof, indx, grp, f, choice.id)
                for t in tot:
                    t['Question'] = questions.text
                    t['Choice'] = choice.text
                    t['id'] = choice.id
                    tot_ret.append(t)
    print(tot_ret)
    return tot_ret


@login_required
def survey_detail_stats(request, pk, sf):

    try:
        survey = Survey.objects.prefetch_related("questions__choices").get(
            Q(id__exact=pk) & Q(creator__exact=request.user) & (Q(status__exact=1) | Q(status__exact=2)))
    except Survey.DoesNotExist:
        raise Http404()

    f_l = ["question", "choice", "civil_state", "education", "working_state", "sex", "country", "city"]
    f_l_1 = ["domanda", "opzione", "stato civile", "educazione", "stato lavorativo", "genere", "paese", "città"]
    new_sf = sf.replace('[', '').replace(']', '').replace("'", '').replace(' ', '')
    f = new_sf.split(',')
    c = 0
    indx = []

    for i in f:
        if i != "0" and i != '':
            indx.append(c)
            print(i)
        c += 1
    print("f:", f)
    print("indx:", indx)

    '''Check if there is a filter on the question'''
    if f[0] != '0':
        q = int(f[0])
    else:
        q = 0
    if f[1] != '0':
        c = int(f[1])
    else:
        c = 0
    if len(indx) == 0:
        messages.info(request, "Nessuna filtro specificato oppure campo opzione specificato senza campo domanda")
        return redirect('polls:survey-detail', pk)

    elif len(indx) == 1:
        if f[0] != "0" or f[1] != "0":
            messages.info(request,  "Nessuna filtro specificato oppure campo opzione specificato senza campo domanda")
            return redirect('polls:survey-detail', pk)
        else:
            grp = [f_l[indx[0]]]
            questions = survey.questions.all()
            choices = survey.choices.all()
            tot_ret = main_survey_loop(indx, 2, questions, grp, f)
    elif len(indx) >= 2:
        grp = []
        for x in indx:
            if x != 0 and x != 1:
                grp.append(f_l[x])
        if f[0] == "0" and f[1] != "0":
            messages.info(request, "Nessuna filtro specificato oppure campo opzione specificato senza campo domanda")
            return redirect('polls:survey-detail', pk)
        if f[0] != "0" and f[1] != "0":
            questions = survey.questions.get(id=int(f[0]))
            tot_ret = main_survey_loop(indx, 1, questions, grp, f)
            choices = questions.choices.get(id=int(f[1]))
        elif f[0] != "0" and f[1] == "0":
            questions = survey.questions.get(id=int(f[0]))
            choices = questions.choices.all()
            tot_ret = main_survey_loop(indx, 1, questions, grp, f)
        elif f[0] == "0" and f[1] == "0":
            questions = survey.questions.all()
            choices = survey.choices.all()
            tot_ret = main_survey_loop(indx, 2, questions, grp, f)

    fields = []
    for i in indx:
        if i != 0 and i != 1:
            fields.append(f_l_1[i])
    fields.append('totale')
    for t in tot_ret:
        if "Question" in t:
            t["domanda"] = t.pop("Question")
        if "Choice" in t:
            t["opzione"] = t.pop("Choice")
        if "sex" in t:
            t["genere"] = t.pop("sex")
        if "civil_state" in t:
            t["stato civile"] = t.pop("civil_state")
        if "education" in t:
            t["educazione"] = t.pop("education")
        if "working_state" in t:
            t["stato lavorativo"] = t.pop("working_state")
        if "country" in t:
            t["paese"] = t.pop("country")
        if "city" in t:
            t["citta"] = t.pop("city")
    return render(
        request,
        "survey_stats.html",
        {
            "survey": survey,
            "questions": questions,
            "choices": choices,
            "stats": tot_ret,
            "fields": fields,
        },
    )


@login_required
def survey_delete(request, pk):
    try:
        survey = Survey.objects.prefetch_related("questions__choices").get(
            pk=pk, creator=request.user)
    except Survey.DoesNotExist:
        raise Http404()
    survey.delete()
    return redirect('polls:survey-list', "All")


@login_required
def survey_stop(request, pk):
    try:
        survey = Survey.objects.prefetch_related("questions__choices").get(
            pk=pk, creator=request.user, status=1)
    except Survey.DoesNotExist:
        raise Http404()
    for sub in Submission.objects.filter(survey=survey.id, is_complete=False):
        sub.delete()
    survey.status = 2
    survey.save()
    return redirect('polls:profile')


@login_required
def delete_stop(request, pk):
    try:
        survey = Survey.objects.prefetch_related("questions__choices").get(
            pk=pk, creator=request.user)
    except Survey.DoesNotExist:
        raise Http404()

    return render(request, "survey_delete_stop.html", {"survey": survey})


@login_required
def edit(request, pk):
    try:
        survey = Survey.objects.prefetch_related("questions__choices").get(
            pk=pk, creator=request.user, status=0)
    except Survey.DoesNotExist:
        raise Http404()

    if request.method == "POST":
        if request.POST['send'] == '1':
            survey.status = 1
            survey.save()
            return redirect('polls:survey-list', slug=survey.category.slug)
        if request.POST['send'] == '2':
            survey.delete()
            return redirect('polls:survey-create')
    else:
        questions = survey.questions.all()
        return render(request, "survey_edit.html", {"survey": survey, "questions": questions})


@login_required
def question_create(request, pk):
    """User can add a question to a draft survey"""
    survey = get_object_or_404(Survey, pk=pk, creator=request.user, status=0)
    if request.method == "POST":
        form = QuestionForm(request.POST)
        if form.is_valid():
            question = form.save(commit=False)
            question.survey = survey
            question.save()
            print("question=", question.type)
            if question.type == "0":
                return redirect('polls:choice-create', survey_pk=pk, question_pk=question.pk)
            if question.type == "1":
                for i in range(5):
                    choice = Choice.objects.create(survey_id=pk, question_id=question.pk)
                    choice.text = str(i+1)
                    choice.save()
                messages.info(request, "Creata domanda con voto da 1-5")
                return redirect('polls:survey-edit', pk)
    else:
        form = QuestionForm()

    return render(request, "question.html", {"survey": survey, "form": form})


@login_required
def question_edit(request, pk, question_pk):
    survey = get_object_or_404(Survey, pk=pk, creator=request.user, status=0)
    question = get_object_or_404(Question, survey_id=pk, id=question_pk)
    if request.method == "POST":
        form = QuestionForm(request.POST)
        print(form.is_valid())
        if form.is_valid():
            quest_1 = question
            question = form.save(commit=False)
            if quest_1.type != question.type:
                chs = quest_1.choices.all()
                chs.delete()
            if quest_1.other_field == 1 and question.other_field == 0:
                chs = quest_1.choices.filter(text="Altro")
                chs.delete()

            question.survey = survey
            question.id = question_pk
            question.save()
            if question.type == "0":
                return redirect('polls:choice-create', survey_pk=pk, question_pk=question.pk)
            if question.type == "1":
                for i in range(5):
                    choice = Choice.objects.create(survey_id=pk, question_id=question.pk)
                    choice.text = str(i+1)
                    choice.save()
                messages.info(request, "Created a Vote Choice from 1 to 5")
                return redirect('polls:survey-edit', pk)
    else:
        form = QuestionForm(initial={
            "text": question.text,
            "type": question.type,
            "other_field": question.other_field,
        })
    return render(request, "question.html", {"survey": survey, "form": form})


@login_required
def question_delete(request, pk, question_pk):
    get_object_or_404(Survey, pk=pk, creator=request.user, status=0)
    question = get_object_or_404(Question, id=question_pk)
    question.delete()
    return redirect('polls:survey-edit', pk)


@login_required
def choice_create(request, survey_pk, question_pk):
    """User can add options to a survey question"""
    survey = get_object_or_404(Survey, pk=survey_pk, creator=request.user, status=0)
    question = Question.objects.get(pk=question_pk)
    if request.method == "POST":
        form = ChoiceForm(request.POST)
        if form.is_valid():
            choice = form.save(commit=False)
            choice.survey_id = survey_pk
            choice.question_id = question_pk
            choice.save()
    else:
        form = ChoiceForm()

    choice = question.choices.all()
    return render(request, "choice.html", {"survey": survey, "question": question, "choice": choice, "form": form})


@login_required
def add_other(request, survey_pk, question_pk):
    question = Question.objects.get(pk=question_pk)
    get_object_or_404(Survey, pk=survey_pk, creator=request.user, status=0)
    if question.other_field:
        choice = Choice.objects.create(survey_id=survey_pk, question_id=question.pk)
        choice.text = "Altro"
        choice.save()
        messages.info(request, "Aggiunto campo Altro alla domanda")
        return redirect('polls:survey-edit', survey_pk)
    else:
        return redirect('polls:survey-edit', survey_pk)


@login_required
def choice_delete(request, survey_pk, question_pk, choice_pk):
    get_object_or_404(Survey, pk=survey_pk, status=0)
    choice = get_object_or_404(Choice, survey_id=survey_pk, question_id=question_pk, pk=choice_pk)
    choice.delete()
    return redirect('polls:choice-create', survey_pk, question_pk)


@login_required
def choice_edit(request, survey_pk, question_pk, choice_pk):
    survey = get_object_or_404(Survey, pk=survey_pk, status=0)
    question = get_object_or_404(Question, survey_id=survey_pk, id=question_pk)

    if request.method == 'POST':
        # choice = question.choices.get(id=choice_pk)
        form = ChoiceForm(request.POST)
        if form.is_valid():
            choice = form.save(commit=False)
            choice.survey_id = survey_pk
            choice.question_id = question_pk
            choice.id = choice_pk
            choice.save()
            return redirect('polls:choice-create', survey_pk, question_pk)
    else:
        choice = question.choices.get(id=choice_pk)
        form = ChoiceForm(initial={
            'text': choice.text
        })
    choice = question.choices.all()
    return render(request, "choice.html", {"survey": survey, "question": question, "choice": choice, "form": form})


def start(request, pk):
    """Survey-taker can start a survey"""
    survey = get_object_or_404(Survey, pk=pk, status=1)
    questions = survey.questions.all()
    if request.method == "POST":
        usr = Submission.objects.filter(user=request.user, survey=survey)
        if usr.exists():
            if usr[0].is_complete:
                messages.info(request, "ERRORE: Hai già una sottomissione in questo sondaggio!")
                return render(request, "start.html", {"survey": survey})
            else:
                sub = usr[0]
            try:
                prof = Profile.objects.get(user=request.user)
            except Profile.DoesNotExist:
                messages.info(request, "ERRORE: Profilo non completato")
                return render(request, "start.html", {"survey": survey})
        else:
            sub = Submission.objects.create(user=request.user, survey=survey)
        return redirect("polls:survey-submit", survey_pk=pk, sub_pk=sub.pk)
    return render(request, "start.html", {"survey": survey, "questions": questions})


def submit(request, survey_pk, sub_pk):
    """Survey-taker submit their completed survey."""
    try:
        survey = Survey.objects.prefetch_related("questions__choices").get(pk=survey_pk, status=1)
    except Survey.DoesNotExist:
        raise Http404()

    try:
        sub = survey.submissions.get(pk=sub_pk, is_complete=False)
    except Submission.DoesNotExist:
        raise Http404()
    Err = False
    questions = survey.questions.all()
    choices = [q.choices.all() for q in questions]
    form_kwargs = {"empty_permitted": False, "choice": choices}
    if request.method == "POST":
        if request.POST:
            AnswerFormSet = formset_factory(AnswerForm, extra=len(questions), formset=BaseAnswerFormSet)
            formset = AnswerFormSet(request.POST, form_kwargs=form_kwargs)
            O_F = formset_factory(OtherChoiceForm, extra=len(questions))
            other_formset = O_F(request.POST)
            print("Submit button")
            if formset.is_valid():
                with transaction.atomic():
                    i = 0

                    for (form, o_form) in zip(formset, other_formset):
                        q_pk = questions[i].pk
                        i += 1
                        print(Err)
                        if Answer.objects.filter(submission_id=sub_pk, question_id=q_pk).exists():
                            if Choice.objects.get(id=form.cleaned_data["choice"]).text == "Altro" \
                                    and o_form.is_valid():
                                if o_form.cleaned_data.get('text') is None:
                                    messages.info(request, "ERRORE: Campo altro scelto ma non compilato")
                                    Err = True
                                else:
                                    print("extra_text:", o_form.cleaned_data.get('text'))
                                    Answer.objects.filter(submission_id=sub_pk, question_id=q_pk).update(
                                        choice=form.cleaned_data["choice"],
                                        extra_text=o_form.cleaned_data.get('text'))
                            else:
                                Answer.objects.filter(submission_id=sub_pk, question_id=q_pk).update(
                                    choice=form.cleaned_data["choice"], extra_text="")
                        else:
                            if Choice.objects.get(id=form.cleaned_data["choice"]).text == "Altro" \
                                    and o_form.is_valid():
                                if o_form.cleaned_data.get('text') is None:
                                    messages.info(request, "ERRORE:Campo altro vuoto")
                                    Err = True
                                    print("err setting:", Err)

                                else:
                                    print("extra_text:", o_form.cleaned_data.get('text'))
                                    answer = Answer.objects.create(submission_id=sub_pk, question_id=q_pk,
                                                                   choice_id=form.cleaned_data["choice"],
                                                                   extra_text=o_form.cleaned_data.get('text'))
                            else:
                                answer = Answer.objects.create(submission_id=sub_pk, question_id=q_pk,
                                                               choice_id=form.cleaned_data['choice'],
                                                               extra_text="")
                            print("post answer:", Err)
                            if not Err:
                                answer.save()
                if not Err:
                    sub.is_complete = True
                    sub.save()
                    survey.subs = Submission.objects.filter(survey=survey_pk, is_complete=True).count()
                    survey.save()
                    return redirect("polls:survey-thanks", pk=survey_pk)

            return redirect('polls:survey-submit', survey_pk, sub_pk)
    else:
        O_FS = formset_factory(OtherChoiceForm, extra=len(questions))
        other_formset = O_FS()
        AnswerFormSet = formset_factory(AnswerForm, extra=len(questions), formset=BaseAnswerFormSet)
        if Answer.objects.filter(submission_id=sub_pk).exists():
            choices = Answer.objects.filter(submission_id=sub_pk)

            formset = AnswerFormSet(form_kwargs=form_kwargs)
        else:
            formset = AnswerFormSet(form_kwargs=form_kwargs)
    if not Err:
        print("render:", Err)
        question_forms = zip(questions, formset, other_formset)
        return render(request, "submit.html", {"survey": survey,
                                               "submission": sub, "question_forms": question_forms,
                                               "formset": formset, "other_formset": other_formset})


@login_required
def view_submission(request, survey_pk, sub_pk):
    """User can view his survey submission"""
    try:
        survey = Survey.objects.prefetch_related("questions__choices").get(
            Q(pk__exact=survey_pk) & (Q(status__exact=1) | Q(status__exact=2)))

    except Survey.DoesNotExist:
        raise Http404()
    try:
        submission = Submission.objects.prefetch_related("answers__choice__answer_set")\
            .get(survey=survey_pk, id=sub_pk, user=request.user)

    except Submission.DoesNotExist:
        raise Http404

    questions = survey.questions.all()
    ans = submission.answers.all()
    # Calculate the results.
    # This is a naive implementation and it could be optimised to hit the database less.
    # See here for more info on how you might improve this code: https://docs.djangoproject.com/en/3.1/topics/db/aggregation/

    q_set = zip(questions, ans)
    description = survey.description
    submission_date = submission.created_at
    return render(
        request,
        "submission_detail.html",
        {
            "survey": survey,
            "description": description,
            "questions": q_set,
            "submission_date": submission_date,
        },
    )


def thanks(request, pk):
    """Survey-taker receives a thank-you message."""
    survey = get_object_or_404(Survey, pk=pk, status=1)
    return render(request, "thanks.html", {"survey": survey})

