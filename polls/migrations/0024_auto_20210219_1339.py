# Generated by Django 2.2.5 on 2021-02-19 12:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0023_auto_20210219_1339'),
    ]

    operations = [
        migrations.AlterField(
            model_name='answer',
            name='extra_text',
            field=models.CharField(max_length=200),
        ),
    ]
