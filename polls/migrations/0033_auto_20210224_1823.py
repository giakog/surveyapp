# Generated by Django 2.2.5 on 2021-02-24 17:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0032_auto_20210222_1822'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='sex',
            field=models.CharField(choices=[('maschio', 'Maschio'), ('femmina', 'Femmina'), ('altro', 'Altro')], max_length=1),
        ),
    ]
